import os
import datetime
from pathlib2 import Path
import matplotlib.pyplot as plt
from wkskel import Skeleton
from lrspy import db, reports

base_path = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set = 'S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')
axon_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('skeletons').joinpath('axons').joinpath('separate')
postsyn_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('skeletons').joinpath('postsynTargets').joinpath('separate')
output_path_base = Path(base_path).joinpath('output').joinpath(data_set)

# Generate Current Output Path
now = datetime.datetime.now()
output_path = output_path_base.joinpath("{:04.0f}{:02.0f}{:02.0f}".format(now.year, now.month, now.day))
output_path.mkdir(parents=True, exist_ok=True)

db = db.Db(db_path)

# Get EP information including soma color code and sort according to y position for correct plot zorder
sig = reports.SomaInnervationGlobal(db, output_path)
df_ep = sig.df_main
df_ep['avg_x_um'] = df_ep['avg_x'] * 0.01124
df_ep['avg_y_um'] = df_ep['avg_y'] * 0.01124
df_ep['avg_z_um'] = df_ep['avg_z'] * 0.032
df_ep.sort_values(['avg_y'], inplace=True)

# Get Postsyn information
df_ps = db.table2df('postsyns')

fig, ax = plt.subplots()
i = 0
for ep_idx, ep_row in df_ep.iterrows():
    i += 1
    print('generating plots ... {}'.format(i/len(df_ep)*100))
    ep_id = df_ep.loc[ep_idx, 'id']
    # Load and plot postysns for given ep_id
    ps_fnames = df_ps['fname'][df_ps['endpoint_id'] == ep_id].values
    for ps_fname in ps_fnames:
        skel = Skeleton(os.path.join(postsyn_path, ps_fname), strict=False)
        ax = skel.plot(view='xz', colors=(0.5, 0.5, 0.5, 1), show=False, ax=ax)
    x_data = df_ep['avg_x_um'][df_ep['id'] == ep_id]
    y_data = df_ep['avg_y_um'][df_ep['id'] == ep_id]
    z_data = df_ep['avg_z_um'][df_ep['id'] == ep_id]
    c_data = df_ep['hue_hex'][df_ep['id'] == ep_id]
    ax.scatter(x_data, z_data, c=c_data, s=20)

ax.set_xlim([250, 650])
ax.set_ylim([0, 700])
ax.set_ylim(ax.get_ylim()[::-1])
plt.savefig(os.path.join(output_path, 'plot_postsyns_somata.pdf'))

