import datetime
from pathlib2 import Path
from lrspy import db, reports, graph

base_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set='S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database')
db_file = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')

# Initialize ORM database object
db = db.Db(db_file)

table_names = ['axons', 'endpoints', 'layers', 'postsyns', 'somata', 'somata_tracings']
for table_name in table_names:
    df = db.table2df(table_name)
    df.to_csv(db_path.joinpath(table_name + '.csv').as_posix())
