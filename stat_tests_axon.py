import os
import numpy as np
import pandas as pd

from lrspy import stats

output_dir = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/output/S1BF/20200204/'

fname_specificity = 'soma_innervation_global_comb_all_all.csv'

fname_conn_green = 'soma_innervation_axon_split_green.csv'
fname_conn_red = 'soma_innervation_axon_split_red.csv'

fname_ax = 'axon_innervation_soma_data.csv'
fname_ax_ss = 'axon_innervation_soma_data_subtract_self.csv'

specificity = np.loadtxt(os.path.join(output_dir, fname_specificity), delimiter=',')

df_green = pd.read_csv(os.path.join(output_dir, fname_conn_green))
df_red = pd.read_csv(os.path.join(output_dir, fname_conn_red))

df_ax = pd.read_csv(os.path.join(output_dir, fname_ax))
df_ax_ss = pd.read_csv(os.path.join(output_dir, fname_ax_ss))

df_green['hits_total'] = 0
df_green['hits_M1'] = 0
df_green['hits_S2'] = 0
df_green['frac_M1'] = 0
df_green['prob'] = 0
df_green['hits_total_ss'] = 0
df_green['hits_M1_ss'] = 0
df_green['hits_S2_ss'] = 0
df_green['frac_M1_ss'] = 0
df_green['prob_ss'] = 0
for idx, row in df_green.iterrows():

    print(row.axon_id, idx/len(df_green))

    counts_green = df_ax['total_ep_counts_green'][df_ax['id'] == row['axon_id']].values[0]
    counts_red = df_ax['total_ep_counts_red'][df_ax['id'] == row['axon_id']].values[0]

    prob = stats.axon_prob(data=specificity, hits_total=counts_green + counts_red, hits_m1=counts_green, n_runs=100000)

    df_green.loc[idx, 'hits_M1'] = counts_green
    df_green.loc[idx, 'hits_S2'] = counts_red
    df_green.loc[idx, 'hits_total'] = counts_green + counts_red
    df_green.loc[idx, 'frac_M1'] = counts_green / (counts_green + counts_red)
    df_green.loc[idx, 'prob'] = prob

    counts_green_ss = df_ax_ss['total_ep_counts_green'][df_ax_ss['id'] == row['axon_id']].values[0]
    counts_red_ss = df_ax_ss['total_ep_counts_red'][df_ax_ss['id'] == row['axon_id']].values[0]

    prob_ss = stats.axon_prob(data=specificity, hits_total=counts_green_ss + counts_red_ss, hits_m1=counts_green_ss, n_runs=100000)

    df_green.loc[idx, 'hits_M1_ss'] = counts_green_ss
    df_green.loc[idx, 'hits_S2_ss'] = counts_red_ss
    df_green.loc[idx, 'hits_total_ss'] = counts_green_ss + counts_red_ss
    df_green.loc[idx, 'frac_M1_ss'] = counts_green_ss / (counts_green_ss + counts_red_ss)
    df_green.loc[idx, 'prob_ss'] = prob_ss

df_green['prob'] = df_green['prob'].round(2)
df_green['frac_M1'] = df_green['frac_M1'].round(2)
df_green['prob_ss'] = df_green['prob_ss'].round(2)
df_green['frac_M1_ss'] = df_green['frac_M1_ss'].round(2)
df_green.set_index(['axon_id'], drop=True, inplace=True)
df_green.to_latex(os.path.join(output_dir, 'stat_tests_axon_green.tex'))
df_green.to_csv(os.path.join(output_dir, 'stat_tests_axon_green.csv'))


df_red['hits_total'] = 0
df_red['hits_M1'] = 0
df_red['hits_S2'] = 0
df_red['frac_S2'] = 0
df_red['prob'] = 0
df_red['hits_total_ss'] = 0
df_red['hits_M1_ss'] = 0
df_red['hits_S2_ss'] = 0
df_red['frac_S2_ss'] = 0
df_red['prob_ss'] = 0
for idx, row in df_red.iterrows():

    print(row.axon_id, idx/len(df_red))

    counts_green = df_ax['total_ep_counts_green'][df_ax['id'] == row['axon_id']].values[0]
    counts_red = df_ax['total_ep_counts_red'][df_ax['id'] == row['axon_id']].values[0]

    prob = stats.axon_prob(data=specificity, hits_total=counts_green + counts_red, hits_s2=counts_red, n_runs=100000)

    df_red.loc[idx, 'hits_M1'] = counts_green
    df_red.loc[idx, 'hits_S2'] = counts_red
    df_red.loc[idx, 'hits_total'] = counts_green + counts_red
    df_red.loc[idx, 'frac_S2'] = counts_red / (counts_green + counts_red)
    df_red.loc[idx, 'prob'] = prob

    counts_green_ss = df_ax_ss['total_ep_counts_green'][df_ax_ss['id'] == row['axon_id']].values[0]
    counts_red_ss = df_ax_ss['total_ep_counts_red'][df_ax_ss['id'] == row['axon_id']].values[0]

    prob_ss = stats.axon_prob(data=specificity, hits_total=counts_green_ss + counts_red_ss, hits_s2=counts_red_ss, n_runs=100000)

    df_red.loc[idx, 'hits_M1_ss'] = counts_green_ss
    df_red.loc[idx, 'hits_S2_ss'] = counts_red_ss
    df_red.loc[idx, 'hits_total_ss'] = counts_green_ss + counts_red_ss
    df_red.loc[idx, 'frac_S2_ss'] = counts_red_ss / (counts_green_ss + counts_red_ss)
    df_red.loc[idx, 'prob_ss'] = prob_ss

df_red['prob'] = df_red['prob'].round(2)
df_red['frac_S2'] = df_red['frac_S2'].round(2)
df_red['prob_ss'] = df_red['prob_ss'].round(2)
df_red['frac_S2_ss'] = df_red['frac_S2_ss'].round(2)
df_red.set_index(['axon_id'], drop=True, inplace=True)
df_red.to_latex(os.path.join(output_dir, 'stat_tests_axon_red.tex'))
df_red.to_csv(os.path.join(output_dir, 'stat_tests_axon_red.csv'))
