import datetime
from pathlib2 import Path
from lrspy import db, reports, graph

base_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set='S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')
axon_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('skeletons').joinpath('axons').joinpath('separate')
postsyn_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('skeletons').joinpath('postsynTargets').joinpath('separate')
output_path_base = Path(base_path).joinpath('output').joinpath(data_set)

# Generate Current Output Path
now = datetime.datetime.now()
output_path = output_path_base.joinpath("{:04.0f}{:02.0f}{:02.0f}".format(now.year, now.month, now.day))
output_path.mkdir(parents=True, exist_ok=True)

# Initialize ORM database object
db = db.Db(db_path)

# Initiate reports
sig = reports.SomaInnervationGlobal(db, output_path)
sig.input_histogram()
sig.postsyn('all')
sig.postsyn('PC')
sig.postsyn('IN')
sig.postsyn('all', density=True, label_color='red')
sig.postsyn('PC', density=True, label_color='red')
sig.postsyn('IN', density=True, label_color='red')
sig.postsyn('all', density=True, label_color='green')
sig.postsyn('PC', density=True, label_color='green')
sig.postsyn('IN', density=True, label_color='green')
sig.specificity_comb('all', 'all')
sig.specificity_comb('all', 'PC')
sig.specificity_comb('all', 'IN')
sig.specificity_comb('unique', 'all')
sig.specificity_comb('unique', 'PC')
sig.specificity_comb('unique', 'IN')

sig.specificity_comb('all', 'all', fractions=True)
sig.specificity_comb('unique', 'all', fractions=True)

sig.specificity_split('all')
sig.specificity_split('unique')

sio = reports.SomaInnervationAxon(db, output_path)
sio.targets_all()

ais = reports.AxonInnervationSoma(db, output_path)
ais.target_specificity(subtract_self=False)
ais.target_specificity(subtract_self=True)
#
ctm = reports.Connectome(db, output_path)
ctm.stats()
ctm.cluster_specificity()
ctm.connectivity_matrix()
ctm.connectivity_graph()

# graph = graph.Graph(db)
# graph.get_communities_louvain()

# axt = reports.AxonTree(db, output_path, axon_path, postsyn_path)
# axt.get_d2_tree_for_axon_id('willie')








