import datetime
from pathlib2 import Path
from lrspy import db, graph

base_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set='S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')
output_path_base = Path(base_path).joinpath('output').joinpath(data_set)

# Generate Current Output Path
now = datetime.datetime.now()
output_path = output_path_base.joinpath("{:04.0f}{:02.0f}{:02.0f}".format(now.year, now.month, now.day))
output_path.mkdir(parents=True, exist_ok=True)

# Initialize ORM database object
db = db.Db(db_path)

graph = graph.Graph(db)
#graph.get_adj_target_nodes(source_id='willie')
# for random_seed in range(0,50):
#     output_path_seed = output_path.joinpath('connectivity_graph_community_seed_{}.png'.format(random_seed)).as_posix()
#     graph.draw_community(random_seed=random_seed, output_path=output_path_seed)
pos = graph.get_layout_tripartite()
graph.draw(pos=pos)