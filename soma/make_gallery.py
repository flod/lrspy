import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from lrspy.db import Db
from wkskel import Skeleton


db_path = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/database/skeletons.db'
nml_path = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somata/somata.nml'
output_path = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/output/S1BF/soma_classification/'

xy_range = [250, 650]
z_range = [700, 0]

db = Db(db_path)
skel = Skeleton(nml_path)

df_somata_tracing = db.table2df('somata_tracings')
df_somata = db.table2df('somata')
df_postsyns = db.table2df('postsyns')
df_axons = db.table2df('axons')
df_layers = db.table2df('layers')
layer_data = df_layers.copy()
layer_data['z'] = layer_data['z'] * 0.032

for idx, soma_row in df_somata.iterrows():

    if idx > 5:
        print(idx)

        ep_id = df_somata['endpoint_id'][idx]
        tracing_names = df_somata_tracing['name'][df_somata_tracing['endpoint_id'] == ep_id]
        tree_inds = []
        for tracing_name in tracing_names.values:
            tree_inds.append(skel.names.index(tracing_name))

        df_tmp = df_postsyns[['id', 'presyn_axon_id']][df_postsyns['endpoint_id'] == ep_id].copy()
        df_tmp.reset_index(inplace=True, drop=True)
        df_tmp['presyn_axon_color'] = None
        df_tmp['presyn_axon_source'] = None
        for idx, row in df_tmp.iterrows():
            df_tmp.loc[idx, 'presyn_axon_color'] = df_axons['label_color'][df_axons['id'] == row.presyn_axon_id].values[0]
            df_tmp.loc[idx, 'presyn_axon_source'] = df_axons['label_source'][df_axons['id'] == row.presyn_axon_id].values[0]

        fig = plt.figure(figsize=(6, 5.5))

        ax_xz = plt.axes([0.1, 0.3, 0.4, 0.65])
        skel.plot(tree_inds=tree_inds, view='xz', ax=ax_xz)
        ax_xz.scatter(soma_row.x*0.01124, soma_row.z*0.032, s=50, c=(0.3, 0.3, 0.6, 0.7), zorder=100)
        for _, row in layer_data.iterrows():
            z = row.z
            ax_xz.plot([xy_range[0], xy_range[1]], [z, z], c=(0.5, 0.5, 0.5, 1), linewidth=0.5)
        ax_xz.set_xlim(xy_range[0], xy_range[1])
        ax_xz.set_ylim(z_range[0], z_range[1])

        ax_yz = plt.axes([0.55, 0.3, 0.4, 0.65])
        skel.plot(tree_inds=tree_inds, view='yz', ax=ax_yz)
        ax_yz.scatter(soma_row.y * 0.01124, soma_row.z * 0.032, s=50, c=(0.3, 0.3, 0.6, 0.7), zorder=100)
        for _, row in layer_data.iterrows():
            z = row.z
            ax_yz.plot([xy_range[0], xy_range[1]], [z, z], c=(0.5, 0.5, 0.5, 1), linewidth=0.5)
        ax_yz.set_xlim(xy_range[0], xy_range[1])
        ax_yz.set_ylim(z_range[0], z_range[1])

        ax_txt1 = plt.axes([0.1, 0.15, 1, 0.05])
        str1 = 'soma id: {}, endpoint id: {}, classified as: '.format(soma_row['id'], ep_id) + soma_row['class']
        ax_txt1.text(0, 0, str1)
        ax_txt1.axis('off')

        ax_txt2 = plt.axes([0.1, 0.1, 1, 0.05])
        sum_S2 = sum(df_tmp['presyn_axon_color'] == 'red')
        sum_S2_unique = len(df_tmp['presyn_axon_id'][df_tmp['presyn_axon_color'] == 'red'].unique())
        str2 = 'S2 inputs: {} ({} unique)'.format(sum_S2, sum_S2_unique)
        ax_txt2.text(0, 0, str2, c=(0.8, 0.2, 0.2, 1))
        ax_txt2.axis('off')

        ax_txt3 = plt.axes([0.1, 0.05, 1, 0.05])
        sum_M1 = sum(df_tmp['presyn_axon_color'] == 'green')
        sum_M1_unique = len(df_tmp['presyn_axon_id'][df_tmp['presyn_axon_color'] == 'green'].unique())
        str3 = 'M1 inputs: {} ({} unique)'.format(sum_M1, sum_M1_unique)
        ax_txt3.text(0, 0, str3, c=(0.2, 0.8, 0.2, 1))
        ax_txt3.axis('off')

        plt.savefig(os.path.join(output_path, (soma_row['class'] + '_EP{:03.0f}'.format(ep_id) + '.pdf')))
        plt.close(fig=fig)






