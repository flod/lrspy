import os
import numpy as np
import pickle
from lrspy.db import Db
from wkskel import Skeleton

dist_th = 5
path_db = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/database/skeletons.db'
path_soma_nml = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somata/somata.nml'
path_soma_nml_missing = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/round3/tasks/tasks_round3.nml'

db = Db(path_db)
df_ep = db.table2df('endpoints')
df_ep = df_ep[df_ep['class'] == 'soma']

skel = Skeleton(path_soma_nml)

df_inds = []
ep_positions = []
for df_idx, df_row in df_ep.iterrows():
    flag = False
    ep_position = np.array(df_row[['avg_x', 'avg_y', 'avg_z']]).astype(float)
    for tree_idx in range(skel.num_trees()):
        if skel.names[tree_idx] == 'soma':
            distance = skel.get_distance_to_nodes(ep_position, tree_idx)[0]
            if distance < dist_th:
                flag = True
    if flag is False:
        df_inds.append(df_idx)
        ep_positions.append(ep_position)

skel_tasks = Skeleton(parameters=skel.parameters)
nodes = skel_tasks.define_nodes_from_positions(np.round(np.array(ep_positions)))
skel_tasks.add_nodes_as_trees(nodes)
skel_tasks.write_nml(path_soma_nml_missing)



