from pathlib2 import Path

from wkskel import Skeleton

from lrspy import db, reports

base_path = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set = 'S1BF'
voxel_size = (11.24, 11.24, 32)

# depth cutoff for soma tasks (um)
depth_cutoff = 250
# distance cutoff for endpoint soma assignment (um)
dist_cutoff = 5

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')
nml_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('skeletons').joinpath('somaClassification').joinpath('round2')
nml_fname_input = 'dummy.nml'
nml_fname_output = 'somata_to_annotate_round2.nml'
output_path_base = Path(base_path).joinpath('output').joinpath(data_set)

# Set Output Paths
output_path = output_path_base.joinpath('soma_classification')

# Initialize ORM database object
db = db.Db(db_path)

# Retrieve Endpoints and Somata Table
df_ep = db.table2df('endpoints')
df_som = db.table2df('somata')

cond_soma = df_ep['class'] == 'soma'
cond_covered = ~df_ep.id.isin(df_som.endpoint_id)
cond_depth = df_ep.avg_z < (depth_cutoff / voxel_size[2] * 1000)

df_ep['to_classify'] = False
df_ep.loc[(cond_soma & cond_covered & cond_depth), 'to_classify'] = True

skel = Skeleton(nml_path.joinpath(nml_fname_input).as_posix())
position_x = df_ep.loc[df_ep['to_classify']==True, 'avg_x'].round().astype(int).tolist()
position_y = df_ep.loc[df_ep['to_classify']==True, 'avg_y'].round().astype(int).tolist()
position_z = df_ep.loc[df_ep['to_classify']==True, 'avg_z'].round().astype(int).tolist()
nodes = skel.get_nodes(position_x, position_y, position_z)

names = df_ep.loc[df_ep['to_classify']==True, 'id'].round().astype(int).tolist()
skel = skel.add_nodes_as_trees(nodes, names=names)

skel.write_nml(nml_path.joinpath(nml_fname_output).as_posix())









