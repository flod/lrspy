import os
import numpy as np
import pickle
from lrspy.db import Db
from wkskel import Skeleton

path_tasks_nml = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/classification/round_3/tasks_round3.nml'
path_tracings = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/classification/round_3/finishedTasks/extracted/'
path_assigments_pickle = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/classification/round_3/assignments.pickle'

path_db = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/database/skeletons.db'
path_postsyns = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/postsynTargets/separate/'

path_class_skeleton_pickle = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/classification/round_3/skel_class.pickle'
path_class_nml = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/classification/round_3/classification.nml'

dist_th = 1#um

#
db = Db()
df_postsyns = db.table2df('postsyns')
#
skel_tasks = Skeleton(path_tasks_nml)
nml_fnames = os.listdir(path_tracings)

force_redo = False
if os.path.exists(path_assigments_pickle) & (not force_redo):
    with open(path_assigments_pickle, 'rb') as f:
        assignments = pickle.load(f)

else:
    assignments = {}
    for fname_idx, nml_fname in enumerate(nml_fnames):
        print('Searching correspondences ... {:03.0f}%'.format(fname_idx/len(nml_fnames)*100))
        skel_trace = Skeleton(os.path.join(path_tracings, nml_fname))
        skel_trace_nodes = np.concatenate([nodes.position.values for nodes in skel_trace.nodes], axis=0)
        for tree_idx, nodes in enumerate(skel_tasks.nodes):
            distances = np.concatenate([skel_trace.get_distance_to_nodes(nodes.position.values, tree_idx) for tree_idx
                                        in range(skel_trace.num_trees())])
            if any(distances < dist_th):
                task_key = '{:03.2f}'.format(tree_idx)
                if task_key not in assignments.keys():
                    assignments[task_key] = [nml_fname]
                else:
                    assignments[task_key].append(nml_fname)



    with open(path_assigments_pickle, 'wb') as f:
        pickle.dump(assignments, f)

force_redo = True
if os.path.exists(path_class_skeleton_pickle) & (not force_redo):
    with open(path_class_skeleton_pickle, 'rb') as f:
        skel_class = pickle.load(f)

else:

    skel_class = Skeleton(parameters=skel_tasks.parameters)
    for task_idx, task_key in enumerate(assignments.keys()):
        print(task_idx)

        # add target group
        skel_class.add_group(id=task_idx+1, name='XX|A|B|C|D')

        # add task tree
        skel_tasks._reset_node_ids(skel_class.max_node_id() + 1)
        nodes = skel_tasks.nodes[int(float(task_key))]
        skel_class.add_tree(nodes=nodes, group_id=task_idx+1, name='soma', color=(1.0, 0.0, 0.0, 1.0))

        # add postsyn trees
        postsyn_inds = np.argwhere(np.sqrt(np.sum((np.asarray((df_postsyns[['endpoint_x', 'endpoint_y', 'endpoint_z']] -
            nodes.position.values)) * np.asarray(skel_class.parameters.scale).reshape(1,3)/1000)**2, axis=1)) < 2).flatten()
        for postsyn_idx in postsyn_inds:
            postsyn_fname = df_postsyns.loc[postsyn_idx, 'fname']
            try:
                skel_ps = Skeleton(os.path.join(path_postsyns, postsyn_fname))
                for tree_idx in range(skel_ps.num_trees()):
                    skel_ps.colors = [(0.0, 1.0, 0.0, 1.0)] * skel_ps.num_trees()
                    skel_class.add_tree_from_skel(skel_ps, tree_idx=tree_idx, group_id=task_idx + 1)
            except:
                print('failed for '+postsyn_fname)

        # add tracing trees
        for trace_idx, trace_item in enumerate(assignments[task_key]):
            skel_trace = Skeleton(os.path.join(path_tracings, trace_item))
            for tree_idx in range(skel_trace.num_trees()):
                skel_trace.group_ids = [trace_idx] * skel_trace.num_trees()
                skel_trace.colors = [(0.0, 0.0, 0.0, 1.0)] * skel_trace.num_trees()
                skel_class.add_tree_from_skel(skel_trace, tree_idx=tree_idx, group_id=task_idx+1)

        # if task_idx == 15:
        #     break


    # with open(path_class_skeleton_pickle, 'wb') as f:
    #     pickle.dump(skel_class, f)

print('write')
skel_class.write_nml(path_class_nml)
