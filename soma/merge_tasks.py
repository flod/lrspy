import numpy as np
from wkskel import Skeleton

#
input_path_round1 = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/round1/tasks/tasks_round1.nml'
input_path_round2 = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/round2/tasks/tasks_round2.nml'
output_path_merged = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/skeletons/somaClassification/merged/tasks_merged.nml'

dist_duplicate = 5#um

#
skel = Skeleton(input_path_round1)
skel.add_tree_from_skel(Skeleton(input_path_round2))

node_pos_all = np.concatenate([nodes.position.values for nodes in skel.nodes], axis=0)
scale = np.asarray(skel.parameters.scale).reshape(1, 3)

tree_inds_dupl = []
for tree_idx, _ in enumerate(skel.nodes):
    skel.colors[tree_idx] = (0.0, 0.0, 0.0, 1.0)
    skel.names[tree_idx] = 'soma'
    node_pos = skel.nodes[tree_idx].position.values
    node_dist_all = np.sqrt(np.sum(((node_pos_all - node_pos) * (scale/1000))**2, axis=1))
    tree_idx_dupl = np.argwhere(node_dist_all < dist_duplicate)
    tree_idx_dupl = np.delete(tree_idx_dupl, np.argwhere(tree_idx_dupl == tree_idx))
    for idx in tree_idx_dupl:
        tree_inds_dupl.append(idx)

tree_ids_dupl = [skel.tree_ids[i] for i in tree_inds_dupl]
for tree_id_dupl in tree_ids_dupl:
    skel.delete_tree(id=tree_id_dupl)

skel.write_nml(output_path_merged)
