from pathlib2 import Path
from lrspy import db

base_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set='S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')

# Initialize ORM database object
db = db.Db(db_path)

endpoint_id = 66
print(db.get_postsyn_id_for_endpoint_id(endpoint_id))