import os
import numpy as np

from ndtest.ndtest import ks2d2s

output_dir = '/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/output/S1BF/20200124/'
fname_ref = 'soma_innervation_global_comb_unique_IN.csv'
fname_tar = 'soma_innervation_global_comb_unique _PC.csv'

path_ref = os.path.join(output_dir, fname_ref)
sm_ref = np.loadtxt(path_ref, delimiter=',')
xy_ref = []
for (i, j), val in np.ndenumerate(sm_ref):
    for z in range(int(val)):
        xy_ref.append([i, j])
xy_ref_np = np.asarray(xy_ref)

num_red = np.sum(np.sum(sm_ref, axis=0) * np.arange(0, sm_ref.shape[1]))
num_green = np.sum(np.sum(sm_ref, axis=1) * np.arange(0, sm_ref.shape[0]))
frac_red = num_red / (num_red + num_green)
frac_green = num_green / (num_red + num_green)
print('num red: {:03.0f}; frac red / S2: {:01.2f}'.format(num_red, frac_red))
print('num green: {:03.0f}; frac green / M1: {:01.2f}'.format(num_green, frac_green))

path_tar = os.path.join(output_dir, fname_tar)
sm_tar = np.loadtxt(path_tar, delimiter=',')
xy_tar = []
for (i, j), val in np.ndenumerate(sm_tar):
    for z in range(int(val)):
        xy_tar.append([i, j])
xy_tar_np = np.asarray(xy_tar)

ks2d2s_p = ks2d2s(xy_ref_np[:, 0], xy_ref_np[:, 1], xy_tar_np[:, 0], xy_tar_np[:, 1])

print('2d 2 sample smirnoff p: {}'.format(ks2d2s_p))

