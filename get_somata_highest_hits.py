from pathlib2 import Path
import datetime
from lrspy import db, reports

base_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity'
data_set='S1BF'

# Set Base Paths
db_path = Path(base_path).joinpath('data').joinpath(data_set).joinpath('database').joinpath('skeletons.db')
output_path_base = Path(base_path).joinpath('output').joinpath(data_set)

# Generate Current Output Path
now = datetime.datetime.now()
output_path = output_path_base.joinpath("{:04.0f}{:02.0f}{:02.0f}".format(now.year, now.month, now.day))

# Initialize ORM database object
db = db.Db(db_path)

sig = reports.SomaInnervationGlobal(db, output_path)

sig.df_main.sort_values(by=['sum_green'], ascending=False).to_csv(output_path.joinpath('somata_highest_hits_green.csv').as_posix())

sig.df_main.sort_values(by=['sum_red'], ascending=False).to_csv(output_path.joinpath('somata_highest_hits_red.csv').as_posix())