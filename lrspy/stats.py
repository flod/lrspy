import numpy as np
from numpy.core.multiarray import ndarray
from scipy import special


def binomial(x, n, p):
    """Compute binomial probability

    Args:
        x (array of ints): Binomial outcomes
        n (int): Total number of draws
        p (array of floats): Binomial probability

    Returns:
        y (float): Resulting binomial probability
    """
    y = multinomial([x, n-x], n, [p, 1-p])
    return y


def multinomial(x, n, p):
    """Compute multinomial probability with arbitrary number of outcomes
    Args:
        x (array of ints): Multinomial outcome
        n (int): Total number of draws
        p (array of floats): Multinomial probabilities

    Returns:
        y (float): Resulting multinomial probability
    """
    x = np.asarray(x)
    p = np.asarray(p)
    y = (special.factorial(n) / np.prod(special.factorial(x))) * np.prod(np.power(p, x))
    return y


def axon_prob(data, hits_total, n_runs, hits_m1=None, hits_s2=None):

    ind_mat_1 = np.tile(np.arange(0, data.shape[0]), (data.shape[1], 1))
    ind_mat_0 = ind_mat_1.T
    ind_mat = ind_mat_0 + ind_mat_1

    ns_m1 = np.zeros(n_runs)
    ns_s2 = np.zeros(n_runs)

    try:
        for idx_run in range(n_runs):
            data_cur = data.copy()
            n_cur = 0
            n_m1 = 0
            n_s2 = 0
            flag = True
            while flag:
                # Determine allowed indices based on number of synapses to assign and availability in data
                n_rem = hits_total - n_cur
                ind_allowed = (ind_mat <= n_rem) & (data_cur > 0)
                # Get linear indices of all allowed indices
                (ind_allowed_0, ind_allowed_1) = np.where(ind_allowed)
                # Pick random index
                idx = np.random.randint(0, len(ind_allowed_0))
                # Select value, remove from current data and append to list
                data_cur[ind_allowed_0[idx], ind_allowed_1[idx]] -= 1
                n_m1 = n_m1 + ind_allowed_0[idx]
                n_s2 = n_s2 + ind_allowed_1[idx]
                n_cur = n_cur + ind_mat[ind_allowed_0[idx], ind_allowed_1[idx]]
                if n_cur >= hits_total:
                    flag = False

            ns_m1[idx_run] = n_m1
            ns_s2[idx_run] = n_s2

        if hits_m1 is not None:
            prob = sum(ns_m1 >= hits_m1) / len(ns_m1)
        else:
            prob = sum(ns_s2 >= hits_s2) / len(ns_s2)

    except:

        prob = None

    return prob