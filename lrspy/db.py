import pandas as pd
import warnings
from pandas import DataFrame
from sqlalchemy import create_engine, Table, MetaData, select, func, Numeric, cast, case, distinct, and_


class Db:
    """ SQLAlchemy-based database class for the long range specificity project sqlite db """

    def __init__(self,
                 db_path='/home/drawitschf/Code/mpi/florian-papers/longRangeSpecificity/data/S1BF/database/skeletons.db'):

        """ Initialize db object by establishing db connectivity and defining object relational mapping via reflection

        Args:
            db_path (pathlib2 path object): Path to sqlite database
        """

        self.db_prefix = 'sqlite:///'

        if type(db_path) is str:
            self.db_path = db_path
        else:
            self.db_path = db_path.as_posix()

        self.db_uri = self.db_prefix + self.db_path
        self.select = select
        self.func = func
        self.Numeric = Numeric
        self.cast = cast
        self.case = case
        self.distinct = distinct

        # Initialize engine and connection
        self.engine = create_engine(self.db_uri)
        self.connection = self.engine.connect()

        # Construct table objects via reflection
        self.axons = Table('axons', MetaData(), autoload=True, autoload_with=self.engine)
        self.postsyns = Table('postsyns', MetaData(), autoload=True, autoload_with=self.engine)
        self.endpoints = Table('endpoints', MetaData(), autoload=True, autoload_with=self.engine)
        self.somata = Table('somata', MetaData(), autoload=True, autoload_with=self.engine)
        self.somata_tracings = Table('somata_tracings', MetaData(), autoload=True, autoload_with=self.engine)
        self.layers = Table('layers', MetaData(), autoload=True, autoload_with=self.engine)

    def table2df(self, table_name, limit=None):
        """ Returns full or partial db table as pandas data frame

        Args:
            table_name (str): Name of table to be returned
            limit (int): Limits the number of table rows returned

        Returns:
            df (pandas DataFrame): Database table as DataFrame
        """

        table = getattr(self, table_name)
        stmt = select([table]).limit(limit)
        results = self.connection.execute(stmt).fetchall()
        df: DataFrame = pd.DataFrame(results)
        df.columns = results[0].keys()

        return df

    def get_endpoint_id_for_endpoint_xyz(self, endpoint_xyz, search_radius_xyz=[100, 100, 30]):
        """ Finds one (or more) endpoint id(s) within search radius to the provided coordinates

        Args:
            endpoint_xyz (list of int): [x, y, z] coordinates specifying endpoint location
            search_radius_xyz (list of int): [x, y, z] search radius (px)

        Returns:
            endpoint_id (list of int)
        """

        columns = [self.endpoints.columns.id]
        stmt = select(columns)
        stmt = stmt.where(and_(
            self.endpoints.columns.avg_x.between(endpoint_xyz[0] - search_radius_xyz[0],
                                                 endpoint_xyz[0] + search_radius_xyz[0]),
            self.endpoints.columns.avg_y.between(endpoint_xyz[1] - search_radius_xyz[1],
                                                 endpoint_xyz[1] + search_radius_xyz[1]),
            self.endpoints.columns.avg_z.between(endpoint_xyz[2] - search_radius_xyz[2],
                                                 endpoint_xyz[2] + search_radius_xyz[2])))
        results = self.connection.execute(stmt).fetchall()
        endpoint_id = [result[0] for result in results]

        return endpoint_id

    def get_postsyn_id_for_endpoint_id(self, endpoint_id):
        """ Finds one or more postsyns associated with specified endpoint id

        Args:
            endpoint_id (int): Endpoint id for which postsyn(s) should be queried

        Returns:
            postsyn_id (list of int)

        """

        assert type(endpoint_id) is int

        columns = [self.postsyns.columns.id]
        stmt = select(columns)
        stmt = stmt.select_from(
            self.endpoints.join(self.postsyns, self.endpoints.columns.id == self.postsyns.columns.endpoint_id))
        stmt = stmt.where(self.endpoints.columns.id == endpoint_id)
        results = self.connection.execute(stmt).fetchall()
        postsyn_id = [result[0] for result in results]

        return postsyn_id

    def get_endpoint_ids_for_axon_id(self, axon_id, unique=False):

        columns = [self.endpoints.columns.id]
        stmt = select(columns)
        stmt = stmt.select_from(self.axons.
                                join(self.postsyns, self.axons.columns.id == self.postsyns.columns.presyn_axon_id).
                                join(self.endpoints, self.postsyns.columns.endpoint_id == self.endpoints.columns.id))
        stmt = stmt.where(self.axons.columns.id == axon_id)
        stmt = stmt.where(self.endpoints.columns['class'] == 'soma')
        results = self.connection.execute(stmt).fetchall()
        endpoint_ids = [result[0] for result in results]

        if unique is True:
            endpoint_ids = list(set(endpoint_ids))

        return endpoint_ids







