from lrspy.db import Db
import pandas as pd
import numpy as np
import random
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import colors
import community

class Graph:

    def __init__(self, db=Db()):

        self.db = db

        # Get endpoints table
        columns = [db.axons.columns.id, db.axons.columns.label_source, db.axons.columns.label_color,
                   db.axons.columns.fname_em, db.postsyns.columns.id, db.postsyns.columns.fname,
                   db.endpoints.columns.id, db.endpoints.columns.avg_x, db.endpoints.columns.avg_y,
                   db.endpoints.columns.avg_z]

        stmt = db.select(columns)
        stmt = stmt.select_from(db.axons.
                                outerjoin(db.postsyns, db.axons.columns.id == db.postsyns.columns.presyn_axon_id).
                                outerjoin(db.endpoints, db.postsyns.columns.endpoint_id == db.endpoints.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        # stmt = stmt.group_by(db.axons.columns.id)
        stmt = stmt.order_by(db.axons.columns.id)

        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df_ep = pd.DataFrame(results)
        col_names = ['axon_id', 'axon_label_source', 'axon_label_color', 'axon_fname_em', 'postsyn_id', 'postsyn_fname',
                     'endpoint_id', 'endpoint_avg_x', 'endpoint_avg_y', 'endpoint_avg_z']
        df_ep.columns = col_names

        # Compute weights
        df_weights = df_ep.groupby(['axon_id', 'endpoint_id', 'axon_label_color'])['endpoint_id'].count().rename('weight')
        df_weights = df_weights.rename_axis(['source', 'target', 'color']).reset_index()

        # Add weighted colors
        # Get discriminable colors shades
        weight_max = df_weights['weight'].max()
        norm = colors.Normalize(vmin=0, vmax=weight_max+1)
        cm_greens = plt.cm.get_cmap('Greens')
        cm_reds = plt.cm.get_cmap('Reds')

        df_weights['color_weight'] = [(0, 0, 0, 0)] * len(df_weights)
        for idx, row in df_weights.iterrows():
            if row['color'] == 'green':
                df_weights.at[idx, 'color_weight'] = cm_greens(norm(df_weights.loc[idx, 'weight']))
            elif row['color'] == 'red':
                df_weights.at[idx, 'color_weight'] = cm_reds(norm(df_weights.loc[idx, 'weight']))

        # Generate graph from edge table
        self.G = nx.from_pandas_edgelist(df_weights, source='source', target='target', edge_attr=True)

        # Write node properties
        node_attrs = {}
        for node in self.G.nodes:
            if type(node) is str:
                node_type = 'source'
                node_color = df_weights['color'][df_weights['source'] == node].values[0]
            if type(node) is int:
                node_type = 'target'
                node_color = np.nan
            node_attrs[node] = {'type': node_type, 'color': node_color}

        nx.set_node_attributes(self.G, node_attrs)



    @staticmethod
    def get_pos_max(pos, node_type):
        pos_max = 0
        for key in pos:
            if type(key) is node_type:
                pos_max = max([pos[key][1], pos_max])
        return pos_max

    @staticmethod
    def make_pos(G,
                 node_str_base=0,
                 node_str_spacing=1,
                 node_num_base=0.5,
                 node_num_spacing=1):

        pos = {}
        for node in G.nodes:
            node_type = type(node)
            pos_max = Graph.get_pos_max(pos, node_type)
            if node_type is str:
                pos[node] = (node_str_base, pos_max + node_str_spacing)
            elif node_type is int:
                pos[node] = (node_num_base, pos_max + node_num_spacing)
        return pos

    def get_graph(self):
        return self.G

    def get_adj_target_ids(self, source_id=None):
        adj_target_ids = list(self.G[source_id].keys())
        return adj_target_ids

    def get_adj_source_ids(self, target_id=None):
        adj_source_ids = list(nx.dfs_predecessors(self.G, source=target_id, depth_limit=1).keys())
        return adj_source_ids

    def get_tree_ids(self, source_id=None, depth_limit=2):
        tree = nx.dfs_tree(self.G, source=source_id, depth_limit=depth_limit)
        tree_ids = list(tree.nodes.keys())
        return tree_ids

    def get_conn_comp(self):
        conn_comp = list(nx.connected_components(self.G))
        return conn_comp

    def get_layout_tripartite(self, center=(5, 10, 15), spacing=(2, 1.5, 2), order=[None, None, None]):

        node_types = nx.get_node_attributes(self.G, 'type')
        node_colors = nx.get_node_attributes(self.G, 'color')
        count_source_red = 0
        count_source_green = 0
        count_target = 0
        for key, val in node_types.items():
            if val == 'source':
                if node_colors[key] == 'green':
                    count_source_green += 1
                elif node_colors[key] == 'red':
                    count_source_red += 1
            elif val == 'target':
                count_target += 1

        vert_max = (count_source_green * spacing[0],
                    count_target * spacing[1],
                    count_source_red * spacing[2])
        vert_min = ((max(vert_max) - count_source_green * spacing[0]) / 2,
                    (max(vert_max) - count_target * spacing[1]) / 2,
                    (max(vert_max) - count_source_red * spacing[2]) / 2)
        vert_pos = (np.linspace(vert_min[0], vert_max[0]+vert_min[0], count_source_green),
                    np.linspace(vert_min[1], vert_max[1]+vert_min[1], count_target),
                    np.linspace(vert_min[2], vert_max[2]+vert_min[2], count_source_red))

        if order[0] is None:
            order[0] = list(range(0, len(vert_pos[0])))

        if order[1] is None:
            order[1] = list(range(0, len(vert_pos[1])))

        if order[2] is None:
            order[2] = list(range(0, len(vert_pos[2])))

        pos = {}
        for node in self.G.nodes:
            if node_types[node] == 'source':
                if node_colors[node] == 'green':
                    pos[node] = (center[0], vert_pos[0][order[0][0]])
                    order[0].pop(0)
                elif node_colors[node] == 'red':
                    pos[node] = (center[2], vert_pos[2][order[2][0]])
                    order[2].pop(0)
            elif node_types[node] == 'target':
                pos[node] = (center[1], vert_pos[1][order[1][0]])
                order[1].pop(0)

        return pos

    def get_communities_louvain(self, plot=False):

        G = self.get_graph()
        partition = community.best_partition(G)

        if plot:
            pos = nx.spring_layout(G)  # compute graph layout
            plt.figure(figsize=(10, 10))
            plt.axis('off')
            nx.draw_networkx_nodes(G, pos, node_size=100, cmap=plt.cm.RdYlBu, node_color=list(partition.values()))
            nx.draw_networkx_edges(G, pos, alpha=0.3)
            plt.show(G)

        return partition

    def draw(self, figsize=(16, 12), output_path=None, **kwargs):

        if 'edge_color' not in kwargs:
            edge_color = [self.G[u][v]['color'] for u, v in self.G.edges]
            kwargs['edge_color'] = edge_color

        if 'width' not in kwargs:
            width = [self.G[u][v]['weight'] for u, v in self.G.edges()]
            kwargs['width'] = width

        if 'node_color' not in kwargs:
            node_color = []
            node_color_dict = {'str': (0.5, 0.5, 0.1, 1), 'int': (0.4, 0.4, 0.8, 1)}
            for node in self.G.nodes:
                if type(node) is str:
                    node_color.append(node_color_dict['str'])
                elif type(node) is int:
                    node_color.append(node_color_dict['int'])
            kwargs['node_color'] = node_color

        if 'node_size' not in kwargs:
            node_size = []
            node_size_dict = {'str': 4, 'int': 4 }
            for node in self.G.nodes:
                if type(node) is str:
                    node_size.append(node_size_dict['str'])
                elif type(node) is int:
                    node_size.append(node_size_dict['int'])
            kwargs['node_size'] = node_size

        if 'with_labels' not in kwargs:
            with_labels = True
            kwargs['with_labels'] = with_labels

        if 'font_size' not in kwargs:
            font_size = 4
            kwargs['font_size'] = font_size

        fig, ax = plt.subplots(figsize=figsize)
        nx.draw(self.G, **kwargs)

        if output_path is not None:
            plt.savefig(output_path)









