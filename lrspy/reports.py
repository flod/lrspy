import os
import shutil
import numpy as np
import pandas as pd
from scipy.stats import kde
from pathlib2 import Path
from scipy.stats import binom
from matplotlib import colors
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from wkskel import Skeleton

from lrspy import util, plot
from lrspy.graph import Graph

plt.rcParams.update({'font.size': 8})

class SomaInnervationGlobal:

    def __init__(self, db, output_path, fraction_bin_edges=[0, 0.25, 0.5, 0.75, 1]):

        """
        :param db: lrspy database object
        :param output_path: parent path to write results
        """
        ##################################################
        # Retrieve postsyn density table
        ##################################################

        columns = [db.postsyns.columns.id, db.postsyns.columns.presyn_axon_id, db.postsyns.columns.endpoint_class,
                   db.postsyns.columns.endpoint_id, db.postsyns.columns.endpoint_x, db.postsyns.columns.endpoint_y,
                   db.postsyns.columns.endpoint_z, db.axons.columns.id, db.axons.columns.label_color,
                   db.axons.columns.label_source, db.somata.columns['class']]
        stmt = db.select(columns)
        stmt = stmt.select_from(db.postsyns.
                                join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id).
                                join(db.somata, db.postsyns.columns.endpoint_id == db.somata.columns.endpoint_id))
        stmt = stmt.where(db.postsyns.columns.endpoint_class == 'soma')
        results = db.connection.execute(stmt).fetchall()
        df_postsyn = pd.DataFrame(results)
        df_postsyn.columns = results[0].keys()

        df_postsyn['hue_hex'] = '#000000'
        df_postsyn.loc[df_postsyn['label_color'] == 'red', ['hue_hex']] = '#AA0000'
        df_postsyn.loc[df_postsyn['label_color'] == 'green', ['hue_hex']] = '#00AA00'

        ##################################################
        # Retrieve soma innervation main table
        ##################################################
        # Aggregate table showing the number of hits from the sources grouped by endpoint node_ids

        columns = [db.endpoints.columns.id, db.endpoints.columns.num_members, db.endpoints.columns.avg_x,
                   db.endpoints.columns.avg_y, db.endpoints.columns.avg_z,
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('green'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_green'),
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('red'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_red'),
                   db.somata.columns['class']
                   ]

        stmt = db.select(columns)
        stmt = stmt.select_from(db.endpoints.
            join(db.postsyns, db.endpoints.columns.id == db.postsyns.columns.endpoint_id).
            join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id).
            outerjoin(db.somata, db.endpoints.columns.id == db.somata.columns.endpoint_id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        stmt = stmt.group_by(db.endpoints.columns.id)
        stmt = stmt.order_by(db.endpoints.columns.id)

        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df_main = pd.DataFrame(results)
        df_main.columns = results[0].keys()

        ##################################################
        # Retrieve auxiliary table
        ##################################################
        # Contains unaggregated endpoint and source data used to determine the number of unique hits from the sources

        columns = [db.endpoints.columns.id.label('endpoint_id'), db.postsyns.columns.id.label('postsyn_id'),
                   db.axons.columns.id.label('axon_id'), db.axons.columns.label_color]
        stmt_base = db.select(columns)
        stmt_base = stmt_base.select_from(db.endpoints.
            join(db.postsyns,db.endpoints.columns.id == db.postsyns.columns.endpoint_id).
            join(db.axons,db.postsyns.columns.presyn_axon_id == db.axons.columns.id))

        df_unique = pd.DataFrame(columns=['id', 'sum_green_unique', 'sum_red_unique'])

        for idx, row in df_main.iterrows():
            ep_id = row.id
            stmt_loop = stmt_base.where(db.endpoints.columns.id == ep_id)
            results = db.connection.execute(stmt_loop).fetchall()
            df_tmp = pd.DataFrame(results)
            df_tmp.columns = results[0].keys()
            df_tmp_red = df_tmp.loc[df_tmp.label_color == 'red']
            sum_red_unique = len(df_tmp_red.axon_id.unique())
            df_tmp_green = df_tmp.loc[df_tmp.label_color == 'green']
            sum_green_unique = len(df_tmp_green.axon_id.unique())
            df_unique = df_unique.append(pd.DataFrame([[ep_id, sum_green_unique, sum_red_unique]],
                                                      columns=['id', 'sum_green_unique', 'sum_red_unique']))

            df_unique = df_unique.astype('float64')

        # Set Index and Join Tables
        df_unique = df_unique.set_index(['id'])
        df_main = df_main.join(df_unique, on='id')


        ##################################################
        # Retrieve layers table
        ##################################################
        columns = [db.layers.columns.layer, db.layers.columns.z]
        stmt = db.select(columns)
        results = db.connection.execute(stmt).fetchall()
        df_layers = pd.DataFrame(results)
        df_layers.columns = results[0].keys()

        ##################################################
        # Generate specificity hues for total sums
        ##################################################
        hm_rgb = util.generate_hue_matrix_rgb()
        self.hm_rgb = hm_rgb
        hm_hex = util.generate_hue_matrix_hex()
        self.hm_hex = hm_hex

        # All
        # Add hues to data frame
        for idx in df_main.index:
            g_idx = df_main.loc[idx, 'sum_green'].astype(int)
            r_idx = df_main.loc[idx, 'sum_red'].astype(int)
            df_main.loc[idx, 'hue_green'] = hm_rgb[0, g_idx, 1]
            df_main.loc[idx, 'hue_red'] = hm_rgb[r_idx, 0, 0]
            hex_str = colors.rgb2hex((df_main.loc[idx, 'hue_red'], df_main.loc[idx, 'hue_green'], 0))
            df_main.loc[idx, 'hue_hex'] = hex_str

        # Generate specificity matrix
        sm = util.generate_specifity_matrix(df_main['sum_green'].values, df_main['sum_red'].values, table_size=(11, 11))
        self.sm = sm

        # Unique
        # Add hues to data frame
        for idx in df_main.index:
            g_idx = df_main.loc[idx, 'sum_green_unique'].astype(int)
            r_idx = df_main.loc[idx, 'sum_red_unique'].astype(int)
            df_main.loc[idx, 'hue_green_unique'] = hm_rgb[0, g_idx, 1]
            df_main.loc[idx, 'hue_red_unique'] = hm_rgb[r_idx, 0, 0]
            hex_str = colors.rgb2hex((df_main.loc[idx, 'hue_red_unique'], df_main.loc[idx, 'hue_green_unique'], 0))
            df_main.loc[idx, 'hue_hex_unique'] = hex_str

        # Generate specificity matrix
        sm_unique = util.generate_specifity_matrix(df_main['sum_green_unique'].values, df_main['sum_red_unique'].values, table_size=(11, 11))
        self.sm_unique = sm_unique

        ##################################################
        # Generate fractions
        ##################################################
        df_main['frac_green'] = df_main['sum_green'] / (df_main['sum_green'] + df_main['sum_red'])
        df_main['frac_red'] = df_main['sum_red'] / (df_main['sum_green'] + df_main['sum_red'])
        df_main['frac_green_unique'] = df_main['sum_green_unique'] / (
                    df_main['sum_green_unique'] + df_main['sum_red_unique'])
        df_main['frac_red_unique'] = df_main['sum_red_unique'] / (
                    df_main['sum_green_unique'] + df_main['sum_red_unique'])

        color_modifier = lambda x: x / max(x)
        hm_frac_rgb = util.generate_hue_matrix_rgb(r_max=len(fraction_bin_edges), g_max=len(fraction_bin_edges),
                                                   color_modifier=color_modifier)
        self.hm_frac_rgb = hm_frac_rgb
        hm_frac_hex = util.generate_hue_matrix_hex(r_max=len(fraction_bin_edges), g_max=len(fraction_bin_edges),
                                                   color_modifier=color_modifier)
        self.hm_frac_hex = hm_frac_hex

        df_main['frac_green_bin'] = np.digitize(df_main.frac_green, fraction_bin_edges) - 1
        df_main['frac_green_unique_bin'] = np.digitize(df_main.frac_green_unique, fraction_bin_edges) - 1
        df_main['frac_red_bin'] = np.digitize(df_main.frac_red, fraction_bin_edges) - 1
        df_main['frac_red_unique_bin'] = np.digitize(df_main.frac_red_unique, fraction_bin_edges) - 1

        # Add hues to data frame
        for idx in df_main.index:
            g_idx = df_main.loc[idx, 'frac_green_bin']
            r_idx = df_main.loc[idx, 'frac_red_bin']
            df_main.loc[idx, 'hue_frac_green'] = hm_frac_rgb[0, g_idx, 1]
            df_main.loc[idx, 'hue_frac_red'] = hm_frac_rgb[r_idx, 0, 0]
            hex_str = colors.rgb2hex((df_main.loc[idx, 'hue_frac_red'], df_main.loc[idx, 'hue_frac_green'], 0))
            df_main.loc[idx, 'hue_frac_hex'] = hex_str

        # Generate specificity matrix
        sm_frac = util.generate_specifity_matrix(df_main['frac_green_bin'].values, df_main['frac_red_bin'].values,
                                                 table_size=(len(fraction_bin_edges), len(fraction_bin_edges)))
        self.sm_frac = sm_frac

        # Add hues to data frame
        for idx in df_main.index:
            g_idx = df_main.loc[idx, 'frac_green_unique_bin']
            r_idx = df_main.loc[idx, 'frac_red_unique_bin']
            df_main.loc[idx, 'hue_frac_unique_green'] = hm_frac_rgb[0, g_idx, 1]
            df_main.loc[idx, 'hue_frac_unique_red'] = hm_frac_rgb[r_idx, 0, 0]
            hex_str = colors.rgb2hex(
                (df_main.loc[idx, 'hue_frac_unique_red'], df_main.loc[idx, 'hue_frac_unique_green'], 0))
            df_main.loc[idx, 'hue_frac_unique_hex'] = hex_str

        # Generate specificity matrix
        sm_frac_unique = util.generate_specifity_matrix(df_main['frac_green_unique_bin'].values, df_main['frac_red_unique_bin'].values,
                                                 table_size=(len(fraction_bin_edges), len(fraction_bin_edges)))
        self.sm_frac_unique = sm_frac_unique

        ##################################################
        # Retrieve total synapse number for the two colors
        ##################################################
        columns = [db.func.count(db.postsyns.columns.id).label('count'), db.axons.columns.label_color]
        stmt = db.select(columns)
        stmt = stmt.select_from(db.postsyns.join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id).
                                join(db.endpoints, db.postsyns.columns.endpoint_id == db.endpoints.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        stmt = stmt.group_by(db.axons.columns.label_color)
        results = db.connection.execute(stmt).fetchall()
        df_tot = pd.DataFrame(results)
        df_tot.columns = results[0].keys()
        synapse_count_red = df_tot['count'].loc[df_tot.label_color == 'red'].values[0]
        synapse_count_green = df_tot['count'].loc[df_tot.label_color == 'green'].values[0]

        ##################################################
        # Compute binomial specificity probabilities
        ##################################################

        df_tmp = pd.DataFrame(columns=['id', 'p_green', 'p_green_unique', 'p_red', 'p_red_unique'])

        theta_green = synapse_count_green / (synapse_count_green + synapse_count_red)
        theta_red = 1 - theta_green

        pm_red = np.flipud(np.rot90(util.generate_binom_prob_matrix(df_main.sum_green.max(), df_main.sum_red.max(), theta_red)))
        self.pm_red = pm_red
        pm_green = util.generate_binom_prob_matrix(df_main.sum_green.max(), df_main.sum_red.max(), theta_green)
        self.pm_green = pm_green

        for idx, row in df_main.iterrows():

                x_green = row.sum_green
                n_green = row.sum_green + row.sum_red
                p_green = 1 - binom.cdf(x_green - 1, n_green, theta_green)

                x_green_unique = row.sum_green_unique
                n_green_unique = row.sum_green_unique + row.sum_red_unique
                p_green_unique = 1 - binom.cdf(x_green_unique - 1, n_green_unique, theta_green)

                x_red = row.sum_red
                n_red = row.sum_green + row.sum_red
                p_red = 1 - binom.cdf(x_red - 1, n_red, theta_red)

                x_red_unique = row.sum_red_unique
                n_red_unique = row.sum_green_unique + row.sum_red_unique
                p_red_unique = 1 - binom.cdf(x_red_unique - 1, n_red_unique, theta_red)

                df_tmp = df_tmp.append(pd.DataFrame([[row.id, p_green, p_green_unique, p_red, p_red_unique]],
                                                    columns=['id', 'p_green', 'p_green_unique', 'p_red', 'p_red_unique']))

        df_main = df_main.join(df_tmp.set_index(['id']), on=['id'])

        ##################################################
        # Order table
        ##################################################
        cols = ['id', 'avg_x', 'avg_y', 'avg_z', 'class', 'num_members', 'sum_red', 'sum_red_unique', 'sum_green',
                'sum_green_unique', 'p_red', 'p_red_unique', 'p_green', 'p_green_unique', 'hue_red', 'hue_red_unique',
                'hue_green', 'hue_green_unique', 'hue_hex', 'hue_hex_unique', 'frac_red', 'frac_red_bin', 'hue_frac_red',
                'frac_red_unique', 'frac_red_unique_bin', 'hue_frac_unique_red', 'frac_green', 'frac_green_bin',
                'hue_frac_green', 'frac_green_unique', 'frac_green_unique_bin', 'hue_frac_unique_green', 'hue_frac_hex',
                'hue_frac_unique_hex']

        df_main = df_main[cols]

        self.output_path = Path(output_path)
        self.db = db
        self.df_postsyn = df_postsyn
        self.df_main = df_main
        self.df_layers = df_layers
        self.axon_count_red = synapse_count_red
        self.axon_count_green = synapse_count_green
        self.fraction_bin_edges = fraction_bin_edges

    def postsyn(self, type_mode='all', density=False, label_color='red'):

        density_params = {}
        if type_mode == 'IN':
            df_postsyn_type = self.df_postsyn.loc[self.df_postsyn['class'] == 'IN', :]
            density_params['vmax'] = 30
        elif type_mode == 'PC':
            df_postsyn_type = self.df_postsyn.loc[self.df_postsyn['class'] != 'IN', :]
            density_params['vmax'] = 30
        else:
            df_postsyn_type = self.df_postsyn
            density_params['vmax'] = 30

        if density is True:
            df_postsyn_type = df_postsyn_type[df_postsyn_type['label_color'] == label_color]

        x_data = df_postsyn_type.endpoint_x * 0.01124
        y_data = df_postsyn_type.endpoint_y * 0.01124
        x_range = [250, 650]
        z_data = -df_postsyn_type.endpoint_z * 0.032
        z_range = [-700, 0]
        layer_data = self.df_layers.copy()
        layer_data['z'] = -layer_data['z'] * 0.032
        c_data = df_postsyn_type.hue_hex

        density_params['nbins_xy'] = (x_range[1] - x_range[0])/50
        density_params['nbins_z'] = (z_range[1] - z_range[0])/50
        if label_color == 'red':
            density_params['cmap'] = plt.cm.Reds
        elif label_color == 'green':
            density_params['cmap'] = plt.cm.Greens

        # Initialize figure
        plt.figure(figsize=(4.5, 5))

        # Soma innervation xz
        ax_xz = plt.axes([0.1, 0.45, 0.35, 0.5])
        ax_xz = plot.soma_specificity_scatter(ax_xz, x_data, x_range, z_data, z_range, layer_data=layer_data,
                                              color_data=c_data, class_data=df_postsyn_type['class'], marker_size=10,
                                              density=density, density_params=density_params)
        ax_xz.set_xlabel('x')
        ax_xz.set_ylabel('z')

        # Soma innervation yz
        ax_xy = plt.axes([0.451, 0.45, 0.35, 0.5])
        ax_xy = plot.soma_specificity_scatter(ax_xy, y_data, x_range, z_data, z_range, layer_data=layer_data,
                                              color_data=c_data, class_data=df_postsyn_type['class'], marker_size=10,
                                              density=density, density_params=density_params)
        ax_xy.set_xlabel('y')
        ax_xy.get_yaxis().set_ticks([])

        # Soma innevation yz
        ax_kde = plt.axes([0.801, 0.45, 0.2, 0.5])
        ax_kde, kdes = plot.soma_specificity_kde(ax_kde, z_data, z_range, c_data, line_width=0.5)

        if density is False:
            kdes_out = np.concatenate([kdes[0].reshape(1, -1), kdes[1].reshape(1, -1)], axis=0)
            fname_kde = 'soma_innervation_postsyn_' + type_mode + '_kde.csv'
            np.savetxt(self.output_path.joinpath(fname_kde).as_posix(), kdes_out, delimiter=",")
            fname = 'soma_innervation_postsyn_' + type_mode + '.pdf'
        else:
            fname = 'soma_innervation_postsyn_' + type_mode + '_density_' + label_color + '.pdf'

        plt.savefig(self.output_path.joinpath(fname).as_posix())

    def specificity_comb(self, specificity_mode='all', type_mode='all', fractions=False):

        """ Soma Innervation Specificity Plot
        Generates a soma innervation specificity plot where soma locations are indicated as scatter points at their
        respective locations in xz and yz projections and innervation specificity is indicated using a color code based
        on a red/green mixture model
        :param specificity_mode: (Str) Specifies which specificity counts to plot. Either 'all' or 'unique'
        :param type_mode (Str) Specifies which neuron types to plot. Either 'all' or 'PC' or 'IN'
        :return: None
        """


        if type_mode == 'IN':
            df_main_type = self.df_main.loc[self.df_main['class'] == 'IN',:]
        elif type_mode == 'PC':
            df_main_type = self.df_main.loc[self.df_main['class'] != 'IN', :]
        else:
            df_main_type = self.df_main


        x_data = df_main_type.avg_x * 0.01124
        y_data = df_main_type.avg_y * 0.01124
        x_range = [250, 650]
        z_data = -df_main_type.avg_z * 0.032
        z_range = [-700, 0]
        layer_data = self.df_layers.copy()
        layer_data['z'] = -layer_data['z'] * 0.032
        hm_rgb = self.hm_rgb


        if specificity_mode == 'all':
            if fractions is True:
                c_data = df_main_type.hue_frac_hex
                sm_frac = util.generate_specifity_matrix(
                    df_main_type['frac_green_bin'].values, df_main_type['frac_red_bin'].values,
                    table_size=(len(self.fraction_bin_edges), len(self.fraction_bin_edges)))
                sm = sm_frac
            else:
                c_data = df_main_type.hue_hex
                sm = util.generate_specifity_matrix(
                    df_main_type['sum_green'].values, df_main_type['sum_red'].values,
                    table_size=(11, 11))
                sm = sm
                synapse_count_red = df_main_type['sum_red'].sum()
                synapse_count_green = df_main_type['sum_green'].sum()
                theta_green = synapse_count_green / (synapse_count_green + synapse_count_red)
                theta_red = 1 - theta_green
                pm_red = np.flipud(np.rot90(util.generate_binom_prob_matrix(hm_rgb.shape[0]-1, hm_rgb.shape[1]-1, theta_red)))
                pm_green = util.generate_binom_prob_matrix(hm_rgb.shape[0]-1, hm_rgb.shape[1]-1, theta_green)

        elif specificity_mode == 'unique':
            if fractions is True:
                c_data = df_main_type.hue_frac_unique_hex
                sm_frac_unique = util.generate_specifity_matrix(
                    df_main_type['frac_green_unique_bin'].values, df_main_type['frac_red_unique_bin'].values,
                    table_size=(len(self.fraction_bin_edges), len(self.fraction_bin_edges)))
                sm = sm_frac_unique
            else:
                c_data = df_main_type.hue_hex_unique
                sm_unique = util.generate_specifity_matrix(
                    df_main_type['sum_green_unique'].values, df_main_type['sum_red_unique'].values,
                    table_size=(11, 11))
                sm = sm_unique
                synapse_count_red = df_main_type['sum_red_unique'].sum()
                synapse_count_green = df_main_type['sum_green_unique'].sum()
                theta_green = synapse_count_green / (synapse_count_green + synapse_count_red)
                theta_red = 1 - theta_green
                pm_red = np.flipud(
                    np.rot90(util.generate_binom_prob_matrix(hm_rgb.shape[0]-1, hm_rgb.shape[1]-1, theta_red)))
                pm_green = util.generate_binom_prob_matrix(hm_rgb.shape[0]-1, hm_rgb.shape[1]-1, theta_green)

        # Initialize figure
        plt.figure(figsize=(6, 5.5))

        # Soma innervation xz
        # ax_xz = plt.axes([0.1, 0.15, 0.25, 0.7])
        ax_xz = plt.axes([0.1, 0.45, 0.3, 0.5])
        ax_xz = plot.soma_specificity_scatter(ax_xz, x_data, x_range, z_data, z_range, layer_data=layer_data,
                                              color_data=c_data, class_data=df_main_type['class'], marker_size=10)
        ax_xz.set_xlabel('x')
        ax_xz.set_ylabel('z')

        # Soma innervation yz
        ax_xy = plt.axes([0.45, 0.45, 0.3, 0.5])
        ax_xy = plot.soma_specificity_scatter(ax_xy, y_data, x_range, z_data, z_range, layer_data=layer_data,
                                              color_data=c_data, class_data=df_main_type['class'], marker_size=10)
        ax_xy.set_yticklabels([])

        # Soma innevation yz
        ax_kde = plt.axes([0.8, 0.45, 0.15, 0.5])
        plot.soma_specificity_kde(ax_kde, z_data, z_range, c_data, line_width=0.5)

        # Color legend
        ax2 = plt.axes([0.05, 0.025, 0.3, 0.3])
        ax2.imshow(hm_rgb)
        for (i, j), z in np.ndenumerate(sm):
            if i+j < 6:
                color = (1, 1, 1)
            else:
                color = (0, 0, 0)
            ax2.text(i, j, '{:0.0f}'.format(z), ha='center', va='center', color=color, size=5,
                     fontweight='bold')
        ax2.set_yticks(np.arange(0, hm_rgb.shape[0], 10))
        ax2.set_yticks(np.arange(0, hm_rgb.shape[0]), minor=True)
        ax2.set_xticks(np.arange(0, hm_rgb.shape[1], 10))
        ax2.set_xticks(np.arange(0, hm_rgb.shape[0]), minor=True)
        ax2.xaxis.tick_top()
        ax2.xaxis.label_position.upper()
        # ax2.set_xlabel('M1 (green)')
        # ax2.set_ylabel('S2 (red)')
        # ax2.set_title('# hits')

        if fractions is False:
            # Probability legend
            ax3 = plt.axes([0.375, 0.025, 0.3, 0.3])
            ax3.imshow(hm_rgb)
            for (i, j), z in np.ndenumerate(pm_red):
                if i + j < 6:
                    color = (1, 1, 1)
                else:
                    color = (0, 0, 0)
                if z == 1:
                    ax3.text(i, j, '{:0.0f}'.format(z), ha='center', va='center', color=color, size=5,
                             fontweight='bold')
                else:
                    ax3.text(i, j, '{:3.2f}'.format(z)[1:], ha='center', va='center', color=color, size=5,
                         fontweight='bold')
            ax3.set_yticks(np.arange(0, hm_rgb.shape[0], 10))
            ax3.set_yticks(np.arange(0, hm_rgb.shape[0]), minor=True)
            ax3.set_xticks(np.arange(0, hm_rgb.shape[1], 10))
            ax3.set_xticks(np.arange(0, hm_rgb.shape[0]), minor=True)
            ax3.set_yticklabels([])
            ax3.xaxis.tick_top()
            ax3.xaxis.label_position.upper()
            # ax3.set_xlabel('M1 (green)')
            # ax3.set_ylabel('S2 (red)')
            # ax3.set_title('binom p red')

            fm = util.generate_binom_freq_matrix(pm_red, sm)
            sm_fm_diff = sm - fm
            ax4 = plt.axes([0.7, 0.025, 0.3, 0.3])
            ax4.matshow(sm_fm_diff.T, cmap=plt.cm.coolwarm, vmin=-5, vmax=5)
            for (i, j), z in np.ndenumerate(sm_fm_diff):
                ax4.text(i, j, '{:0.0f}'.format(z), ha='center', va='center', color=(0, 0, 0), size=5,
                         fontweight='bold')
            ax4.set_yticks(np.arange(0, hm_rgb.shape[0], 10))
            ax4.set_yticks(np.arange(0, hm_rgb.shape[0]), minor=True)
            ax4.set_xticks(np.arange(0, hm_rgb.shape[1], 10))
            ax4.set_xticks(np.arange(0, hm_rgb.shape[0]), minor=True)
            ax4.set_yticklabels([])
            ax4.xaxis.tick_top()
            ax4.xaxis.label_position.upper()
            # ax4.set_xlabel('M1 (green)')
            # ax4.set_ylabel('S2 (red)')
            # ax4.set_title('binom p green')

        if fractions is False:
            fname_fm = 'soma_innervation_global_comb_' + specificity_mode + '_' + type_mode + '_pred.csv'
            np.savetxt(self.output_path.joinpath(fname_fm).as_posix(), fm, delimiter=',')
            fname_sm = 'soma_innervation_global_comb_' + specificity_mode + '_' + type_mode + '.csv'
            fname = 'soma_innervation_global_comb_' + specificity_mode + '_' + type_mode + '.pdf'
        else:
            fname_sm = 'soma_innervation_global_comb_' + specificity_mode + '_' + type_mode + '_fractions.csv'
            fname = 'soma_innervation_global_comb_' + specificity_mode + '_' + type_mode + '_fractions.pdf'

        np.savetxt(self.output_path.joinpath(fname_sm).as_posix(), sm, delimiter=',')
        plt.tight_layout()
        plt.savefig(self.output_path.joinpath(fname).as_posix())


    def specificity_split(self, mode='all'):

        """ Soma Innervation Specificity Split Plot
        Generates a matrix of soma innervation specificity plots where soma locations are indicated as scatter points at
        their respective locations in a xz projections and innervation specificity is indicated using a color code based
        on a red/green mixture model
        :param mode: (Str) Specifies which specificity counts to plot. Either 'all' or 'unique'
        :return: None
        """

        xy_data = self.df_main.avg_x * 0.01124
        z_data = -self.df_main.avg_z * 0.032
        xy_range = [250, 650]
        z_range = [-700, 0]
        marker_size = 1
        line_width = 0.25
        hm_hex = self.hm_hex

        if mode == 'all':
            c_data = self.df_main.hue_hex
        elif mode == 'unique':
            c_data = self.df_main.hue_hex_unique

        # Initialize figure
        plt.figure(figsize=(4, 3.2))

        # Initialize axes
        ax_grid = plot.soma_specificity_axes_grid(hm_hex.shape[0]+1, hm_hex.shape[1]+1, spacing_x=0.05, spacing_y=0.05,
                                                  split_x=True, split_x_ratio=0.8)

        plot.soma_specificity_scatter(ax_grid[0, 0], xy_data, xy_range, z_data, z_range, color_data=c_data,
                                      class_data=self.df_main['class'], marker_size=marker_size, linewidths=line_width)
        plot.soma_specificity_kde(ax_grid[0, 1], z_data, z_range, c_data, line_width)

        # Plot separate
        for idx, _ in np.ndenumerate(hm_hex):
            if mode == 'all':
                df_hue = self.df_main.loc[self.df_main.hue_hex == hm_hex[idx[0], idx[1]].astype(str), :]
                c_data = df_hue.hue_hex
            elif mode == 'unique':
                df_hue = self.df_main.loc[self.df_main.hue_hex_unique == hm_hex[idx[0], idx[1]].astype(str), :]
                c_data = df_hue.hue_hex_unique

            xy_data = df_hue.avg_x * 0.01124
            z_data = -df_hue.avg_z * 0.032

            plot.soma_specificity_scatter(ax_grid[idx[0], idx[1] * 2], xy_data, xy_range, z_data, z_range, color_data=c_data,
                                          class_data=df_hue['class'], marker_size=marker_size, linewidths=line_width)
            plot.soma_specificity_kde(ax_grid[idx[0], idx[1] * 2 + 1], z_data, z_range, c_data, line_width)

        if mode == 'all':
            fname = 'soma_innervation_global_split.pdf'
        elif mode == 'unique':
            fname = 'soma_innervation_global_split_unique.pdf'

        plt.savefig(self.output_path.joinpath(fname).as_posix())

    def input_histogram(self):

        fig, ax = plt.subplots(1, 1, figsize=(4, 3))
        ax.hist(self.df_main['num_members'], bins=list(range(1, 11)), log=True, rwidth=0.9, align='left')
        ax.set_xlabel('Number of long-range synapses \n per target neuron')
        ax.set_ylabel('Counts')
        fname = 'soma_innervation_hist.pdf'
        plt.tight_layout()
        plt.savefig(self.output_path.joinpath(fname).as_posix())

class SomaInnervationAxon:

    def __init__(self, db, output_path):

        # Query 1
        columns = [db.endpoints.columns.id, db.endpoints.columns.num_members, db.endpoints.columns.avg_x,
                   db.endpoints.columns.avg_y, db.endpoints.columns.avg_z,
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('green'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_green'),
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('red'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_red')]

        stmt = db.select(columns)
        stmt = stmt.select_from(
            db.endpoints.join(db.postsyns, db.endpoints.columns.id == db.postsyns.columns.endpoint_id).
                join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        stmt = stmt.group_by(db.endpoints.columns.id)
        stmt = stmt.order_by(db.endpoints.columns.id)

        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df = pd.DataFrame(results)
        df.columns = results[0].keys()

        # Generate hue matrix
        hm_rgb = util.generate_hue_matrix_rgb(df.sum_red.max(), df.sum_green.max())
        hm_hex = util.generate_hue_matrix_hex(df.sum_red.max(), df.sum_green.max())

        # Add hues to data frame
        for idx in df.index:
            g_idx = df.loc[idx, 'sum_green']
            r_idx = df.loc[idx, 'sum_red']
            df.loc[idx, 'hue_g'] = hm_rgb[0, g_idx, 1]
            df.loc[idx, 'hue_r'] = hm_rgb[r_idx, 0, 0]
            hex_str = colors.rgb2hex((df.loc[idx, 'hue_r'], df.loc[idx, 'hue_g'], 0))
            df.loc[idx, 'hue_hex'] = hex_str

        # Query 2
        columns = [db.axons.columns.id, db.axons.columns.label_color, db.postsyns.columns.endpoint_id]

        stmt = db.select(columns)
        stmt = stmt.select_from(db.axons.join(db.postsyns, db.axons.columns.id == db.postsyns.columns.presyn_axon_id).
                join(db.endpoints, db.postsyns.columns.endpoint_id == db.endpoints.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')

        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df2 = pd.DataFrame(results)
        df2.columns = results[0].keys()

        ax_ep = {}
        ax_col = {}
        ids_unique = df2.id.unique()
        for id_unique in ids_unique:
            ax_ep[id_unique] = df2.loc[df2.id == id_unique, :].endpoint_id.values
            ax_col[id_unique] = str(df2.loc[df2.id == id_unique, :].label_color.unique()[0])
            pass

        self.output_path = Path(output_path)
        self.db = db
        self.df = df
        self.df2 = df2


    def targets_all(self):

        ax_ep = {}
        ax_col = {}
        ids_unique = self.df2.id.unique()
        for id_unique in ids_unique:
            ax_ep[id_unique] = self.df2.loc[self.df2.id == id_unique, :].endpoint_id.values
            ax_col[id_unique] = str(self.df2.loc[self.df2.id == id_unique, :].label_color.unique()[0])
            pass

        xy_range = [250, 650]
        z_range = [-700, 0]
        marker_size = 15

        ctm = Connectome(self.db, self.output_path)
        layer_data = self.db.table2df('layers')
        layer_data['z'] = -layer_data['z'].copy() * 0.032

        n_rows = 5
        n_cols = 7

        fig, axs = plt.subplots(n_rows, n_cols, figsize=(7, 8))
        for idx2, _ in np.ndenumerate(axs):
            axs[idx2].get_xaxis().set_ticks([])
            axs[idx2].get_yaxis().set_ticks([])

        df_green = pd.DataFrame(ctm.df_conn_green.index)
        df_green.set_index(['axon_id'], drop=True, inplace=True)
        df_green['cluster_id'] = 0
        for idx, id in enumerate(ctm.df_conn_green.index):
            idx2 = np.unravel_index(idx, (n_rows, n_cols))
            df_ax = self.df.loc[self.df.id.isin(ax_ep[id]), :]
            xy_data = df_ax.avg_x * 0.01124
            z_data = -df_ax.avg_z * 0.032
            c_data = df_ax.hue_hex

            ax = plot.soma_specificity_scatter(axs[idx2], xy_data, xy_range, z_data, z_range, color_data=c_data,
                                               marker_size=marker_size, layer_data=layer_data, static_lims=True)

            if ax_col[id] == 'green':
                color = (0, 0.8, 0)
            else:
                color = (0.8, 0, 0)

            sum_green = df_ax['sum_green'].sum()
            sum_red = df_ax['sum_red'].sum()
            title = id + ' ({}|{})'.format(sum_green, sum_red)
            ax.set_title(title, fontsize=7, color=color, y=0.95)

            cluster_id = '{:02.0f}'.format(ctm.df_ax['community_id'][ctm.df_ax['axon_id'] == id].values[0])
            df_green.loc[id, 'cluster_id'] = ctm.df_ax['community_id'][ctm.df_ax['axon_id'] == id].values[0]
            ax.text(580, -670, cluster_id, fontsize=6)

        df_green.to_csv(self.output_path.joinpath('soma_innervation_axon_split_green.csv').as_posix())
        plt.savefig(self.output_path.joinpath('soma_innervation_axon_split_green.pdf').as_posix())


        fig, axs = plt.subplots(n_rows, n_cols, figsize=(7, 8))
        for idx2, _ in np.ndenumerate(axs):
            axs[idx2].get_xaxis().set_ticks([])
            axs[idx2].get_yaxis().set_ticks([])

        df_red = pd.DataFrame(ctm.df_conn_red.index)
        df_red.set_index(['axon_id'], drop=True, inplace=True)
        df_red['cluster_id'] = 0
        for idx, id in enumerate(ctm.df_conn_red.index):
            idx2 = np.unravel_index(idx, (n_rows, n_cols))
            df_ax = self.df.loc[self.df.id.isin(ax_ep[id]), :]
            xy_data = df_ax.avg_x * 0.01124
            z_data = -df_ax.avg_z * 0.032
            c_data = df_ax.hue_hex

            ax = plot.soma_specificity_scatter(axs[idx2], xy_data, xy_range, z_data, z_range, color_data=c_data,
                                               marker_size=marker_size, layer_data=layer_data, static_lims=True)

            if ax_col[id] == 'green':
                color = (0, 0.8, 0)
            else:
                color = (0.8, 0, 0)

            sum_green = df_ax['sum_green'].sum()
            sum_red = df_ax['sum_red'].sum()
            title = id + ' ({}|{})'.format(sum_green, sum_red)
            ax.set_title(title, fontsize=7, color=color, y=0.95)

            cluster_id = '{:02.0f}'.format(ctm.df_ax['community_id'][ctm.df_ax['axon_id'] == id].values[0])
            df_red.loc[id, 'cluster_id'] = ctm.df_ax['community_id'][ctm.df_ax['axon_id'] == id].values[0]
            ax.text(580, -670, cluster_id, fontsize=6)

        df_red.to_csv(self.output_path.joinpath('soma_innervation_axon_split_red.csv').as_posix())
        plt.savefig(self.output_path.joinpath('soma_innervation_axon_split_red.pdf').as_posix())


class AxonInnervationSoma:

    def __init__(self, db, output_path):

        # Query endpoints with total innervation type numbers
        columns = [db.endpoints.columns.id, db.endpoints.columns.num_members, db.endpoints.columns.avg_x,
                   db.endpoints.columns.avg_y, db.endpoints.columns.avg_z,
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('green'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_green'),
                   db.func.sum(
                       db.cast(db.axons.columns.label_color.is_('red'), db.Numeric(2, 0, asdecimal=False))).label(
                       'sum_red')]
        stmt = db.select(columns)
        stmt = stmt.select_from(db.endpoints.
                                join(db.postsyns, db.endpoints.columns.id == db.postsyns.columns.endpoint_id).
                                join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        stmt = stmt.group_by(db.endpoints.columns.id)
        stmt = stmt.order_by(db.endpoints.columns.id)
        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df = pd.DataFrame(results)
        df.columns = results[0].keys()
        # Add fractions
        df['frac_green'] = df['sum_green']/(df['sum_green'] + df['sum_red'])
        df['frac_red'] = df['sum_red'] / (df['sum_green'] + df['sum_red'])
        df_ep = df

        # Query axons
        columns = [db.axons.columns.id, db.axons.columns.label_source, db.axons.columns.label_color]
        stmt = db.select(columns)
        stmt = stmt.select_from(db.axons)
        stmt = stmt.order_by(db.axons.columns.id)
        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df = pd.DataFrame(results)
        df.columns = results[0].keys()
        df_ax = df

        # Query all endpoint targets for all axons
        columns = [db.axons.columns.id, db.axons.columns.label_color, db.endpoints.columns.id]
        stmt = db.select(columns)
        stmt = stmt.select_from(db.axons.
                                join(db.postsyns, db.axons.columns.id == db.postsyns.columns.presyn_axon_id).
                                join(db.endpoints, db.postsyns.columns.endpoint_id == db.endpoints.columns.id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        stmt = stmt.order_by(db.axons.columns.id)
        # Execute db query and retrieve results as data frame
        results = db.connection.execute(stmt).fetchall()
        df = pd.DataFrame(results)
        df.columns = ['ax_id', 'label_color', 'ep_id']
        df_ax_ep = df

        # Attach to object
        self.df_ep = df_ep
        self.df_ax = df_ax
        self.df_ax_ep = df_ax_ep
        self.output_path = output_path

    def target_specificity(self, subtract_self=False):

        self.df_ax['total_ep_counts_red'] = 0
        self.df_ax['total_ep_counts_green'] = 0

        for axon_id in self.df_ax.id:
            ep_ids = list(set(self.df_ax_ep.ep_id[self.df_ax_ep.ax_id == axon_id]))
            for ep_id in ep_ids:
                self.df_ax.loc[self.df_ax.id == axon_id, 'total_ep_counts_red'] += \
                    self.df_ep.sum_red[self.df_ep.id == ep_id].values[0]
                self.df_ax.loc[self.df_ax.id == axon_id, 'total_ep_counts_green'] += \
                    self.df_ep.sum_green[self.df_ep.id == ep_id].values[0]
            if subtract_self is True:
                self.df_ax.loc[self.df_ax.id == axon_id, 'total_ep_counts_red'] -= \
                    sum((self.df_ax_ep['ax_id'] == axon_id) & (self.df_ax_ep['label_color'] == 'red'))
                self.df_ax.loc[self.df_ax.id == axon_id, 'total_ep_counts_green'] -= \
                    sum((self.df_ax_ep['ax_id'] == axon_id) & (self.df_ax_ep['label_color'] == 'green'))

        self.df_ax['total_ep_frac_red'] = self.df_ax['total_ep_counts_red'] / \
                                          (self.df_ax['total_ep_counts_red'] + self.df_ax['total_ep_counts_green'])
        self.df_ax['total_ep_frac_green'] = self.df_ax['total_ep_counts_green'] / \
                                          (self.df_ax['total_ep_counts_red'] + self.df_ax['total_ep_counts_green'])

        glob_max = max([self.df_ax.total_ep_counts_red.max(), self.df_ax.total_ep_counts_green.max()])
        color_modifier = lambda x: (1/(1+np.exp(-x/13)) - 0.5) * 2
        img = util.generate_hue_matrix_rgb(r_max=glob_max, g_max=glob_max, color_modifier=color_modifier)

        # Make scatter plot showing the total numbers of source specific innervations the targeted somata of specific
        # axons receive -> each scatter point represents one axon, the position of the scatter point represents how
        # specific its target somata are

        cm = plt.get_cmap('gist_ncar')
        cmd = cm(np.linspace(0, 1, len(self.df_ax)))

        label_color_map = {'green': (0, 1, 0, 1), 'red': (1, 0, 0, 1)}

        # Make 2d scatter plot showing individual axons
        fig, ax = plt.subplots()
        for idx, row in self.df_ax.iterrows():
            ax.scatter(row.total_ep_counts_green, row.total_ep_counts_red, c=np.reshape(label_color_map[row.label_color], (1,-1)), s=5,
                       edgecolors=np.reshape(cmd[idx], (1,-1)), linewidths=0.5, label=row.id, zorder=2)
            ax.text(row.total_ep_counts_green + 1, row.total_ep_counts_red, s=row.id, fontsize=4, zorder=1)
        ax.imshow(img, alpha=0.7)
        ax.set_xlim(-1, glob_max+1)
        ax.set_ylim(-1, glob_max+1)
        ax.set_xticks(list(range(glob_max+1)), minor=True)
        ax.set_yticks(list(range(glob_max + 1)), minor=True)
        ax.set_aspect('equal')
        ax.legend(fontsize=5, ncol=2)
        plt.xlabel('total number of M1 hits received by targets of given axon')
        plt.ylabel('total number of S2 hits received by targets of given axon')
        if subtract_self is True:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_scatter_subtract_self.pdf').as_posix())
        else:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_scatter.pdf').as_posix())

        # Make 2d density plot
        bins, _, _ = np.histogram2d(self.df_ax['total_ep_counts_green'], self.df_ax['total_ep_counts_red'], bins=range(
            max(self.df_ax['total_ep_counts_green'].max(), self.df_ax['total_ep_counts_red'].max())))
        bins_max = np.max(bins)
        fig, ax = plt.subplots()
        h = ax.hist2d(self.df_ax['total_ep_counts_green'], self.df_ax['total_ep_counts_red'],
                  bins=[self.df_ax['total_ep_counts_green'].max(), self.df_ax['total_ep_counts_red'].max()],
                  cmap=plt.cm.Blues)
        ax.set_xlim(0, glob_max+1)
        ax.set_ylim(0, glob_max+1)
        ax.set_xticks(list(range(glob_max+1)), minor=True)
        ax.set_yticks(list(range(glob_max + 1)), minor=True)
        ax.set_aspect('equal')
        plt.xlabel('total number of M1 hits received by targets of given axon')
        plt.ylabel('total number of S2 hits received by targets of given axon')
        plt.colorbar(h[3], ax=ax, ticks=list(range(int(bins_max)+1)), values=list(range(int(bins_max)+1)))
        if subtract_self is True:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_density_subtract_self.pdf').as_posix())
        else:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_density.pdf').as_posix())

        # Make histogram showing the total fractional specificity for all targets of the axons
        fig, ax = plt.subplots()
        ax.hist(self.df_ax.total_ep_frac_red)
        plt.xlabel('total fraction of S2 hits received by targets of given axon')
        plt.ylabel('counts')
        if subtract_self is True:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_hist_S2_subtract_self.pdf').as_posix())
        else:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_hist_S2.pdf').as_posix())

        # Make histogram showing the total fractional specificity for all targets of the axons
        fig, ax = plt.subplots()
        ax.hist(self.df_ax.total_ep_frac_green)
        plt.xlabel('total fraction of M1 hits received by targets of given axon')
        plt.ylabel('counts')
        if subtract_self is True:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_hist_M1_subtract_self.pdf').as_posix())
        else:
            plt.savefig(self.output_path.joinpath('axon_innervation_soma_hist_M1.pdf').as_posix())

        if subtract_self is True:
            self.df_ax.to_csv(self.output_path.joinpath('axon_innervation_soma_data_subtract_self.csv'))
        else:
            self.df_ax.to_csv(self.output_path.joinpath('axon_innervation_soma_data.csv'))


class AxonTree:

    def __init__(self, db, output_path, axon_path, postsyn_path):

        graph = Graph(db)
        self.graph = graph
        self.db = db
        self.output_path=output_path
        self.axon_path=axon_path
        self.postsyn_path=postsyn_path

    def get_axon_em_path(self, axon_id):
        axon_em_path = self.axon_path.joinpath(axon_id+'_em.nml')
        return axon_em_path

    def get_postsyn_path(self, postsyn_id):
        postsyn_path = self.postsyn_path.joinpath(postsyn_id+'.nml')
        return postsyn_path

    def get_d2_tree_for_axon_id(self, axon_id, write_plot=True, write_nmls=True):

        output_path_axon_id = self.output_path.joinpath('axon_tree').joinpath(axon_id)
        os.makedirs(output_path_axon_id, exist_ok=True)

        ep_ids = self.graph.get_adj_target_ids(axon_id)
        ax_ids = []
        ps_ids = []
        for ep_id in ep_ids:
            ax_ids_ep = self.graph.get_adj_source_ids(ep_id)
            for ax_id_ep in ax_ids_ep:
                if ax_id_ep not in ax_ids:
                    ax_ids.append(ax_id_ep)
            ps_ids_ep = self.db.get_postsyn_id_for_endpoint_id(ep_id)
            for ps_id_ep in ps_ids_ep:
                if ps_id_ep not in ps_ids:
                    ps_ids.append(ps_id_ep)

        ax_paths = []
        for ax_id in ax_ids:
            ax_path = self.get_axon_em_path(ax_id).as_posix()
            ax_paths.append(ax_path)
            if write_nmls:
                target_path = output_path_axon_id.joinpath(os.path.basename(ax_path))
                shutil.copyfile(ax_path, target_path)

        ps_paths = []
        for ps_id in ps_ids:
            ps_path = self.get_postsyn_path(ps_id).as_posix()
            ps_paths.append(ps_path)
            if write_nmls:
                target_path = output_path_axon_id.joinpath(os.path.basename(ps_path))
                shutil.copyfile(ps_path, target_path)


        sig = SomaInnervationGlobal(self.db, self.output_path)
        df_ep = sig.df_main
        df_ax = self.db.table2df('axons')

        fig, axs = plt.subplots(1,2)

        ax_colors = {'red': (1, 0, 0, 1), 'green': (0, 1, 0, 1)}
        for ax_id, ax_path in zip(ax_ids, ax_paths):
            skel = Skeleton(ax_path)
            ax_color = ax_colors[df_ax.label_color[df_ax.id == ax_id].values[0]]
            axs[0] = skel.plot(colors=[ax_color], ax=axs[0])

        for ps_id, ps_path in zip(ps_ids, ps_paths):
            skel = Skeleton(ps_path)
            axs[0] = skel.plot(ax=axs[0])

        axs[1] = axs[0]
        axs[0].view_init(elev=-90, azim=-90)
        axs[1].view_init(elev=180, azim=-90)


class Connectome:

    def __init__(self, db, output_path):

        self.db = db
        self.output_path = output_path

        # Get graph and partition for connectome clustering
        graph = Graph(db=self.db)
        c = graph.get_communities_louvain()

        # Get axon df
        columns = [db.axons.columns.id, db.axons.columns.label_source, db.axons.columns.label_color,
                   db.func.count(db.axons.columns.id)]

        stmt = db.select(columns)
        stmt = stmt.select_from(db.postsyns.
                                join(db.axons, db.postsyns.columns.presyn_axon_id == db.axons.columns.id))
        stmt = stmt.group_by(db.postsyns.columns.presyn_axon_id)
        stmt = stmt.where(db.postsyns.columns.endpoint_class == 'soma')
        results = db.connection.execute(stmt).fetchall()
        df_ax = pd.DataFrame(results)
        df_ax.columns = ['axon_id', 'axon_label_source', 'axon_label_color', 'synapse_count']
        df_ax['community_id'] = 0
        for idx, row in df_ax.iterrows():
            df_ax.loc[idx, 'community_id'] = c[row['axon_id']]
        df_ax = df_ax.sort_values(['axon_label_source', 'community_id'], ascending=True)

        # Get endpoint df
        columns = [db.endpoints.columns.id, db.endpoints.columns.avg_x, db.endpoints.columns.avg_y,
                   db.endpoints.columns.avg_z, db.somata.columns['class']]

        stmt = db.select(columns)
        stmt = stmt.select_from(db.endpoints.
                                outerjoin(db.somata, db.endpoints.columns.id == db.somata.columns.endpoint_id))
        stmt = stmt.where(db.endpoints.columns['class'] == 'soma')
        results = db.connection.execute(stmt).fetchall()
        df_ep = pd.DataFrame(results)
        df_ep.columns = ['endpoint_id', 'endpoint_avg_x',
                        'endpoint_avg_y', 'endpoint_avg_z', 'somata_class']
        df_ep = df_ep.replace(to_replace=[None], value='PC')
        df_ep = df_ep.replace(to_replace=['None'], value='PC')
        df_ep['community_id'] = 0
        for idx, row in df_ep.iterrows():
            df_ep.loc[idx, 'community_id'] = c[row['endpoint_id']]
        df_ep = df_ep.sort_values(['somata_class', 'community_id'], ascending=[False, True])

        # Generate connectome df
        data = np.zeros((len(df_ax), len(df_ep)))
        columns = pd.MultiIndex.from_frame(df_ep[['somata_class', 'endpoint_id']])
        df_conn = pd.DataFrame(data=data, index=df_ax.axon_id, columns=columns)

        # Populate connectome df
        for axon_id, row in df_conn.iterrows():
            ep_ids = db.get_endpoint_ids_for_axon_id(axon_id)
            for ep_id in ep_ids:
                df_conn.loc[axon_id, df_conn.columns.get_level_values(1) == ep_id] += 1

        ind_red = df_ax['axon_label_color'] == 'red'
        df_conn_red = df_conn[ind_red.values]
        ind_green = df_ax['axon_label_color'] == 'green'
        df_conn_green = df_conn[ind_green.values]

        self.g = graph
        self.c = c
        self.df_ax = df_ax
        self.df_ep = df_ep
        self.df_conn = df_conn
        self.ind_red = ind_red
        self.ind_green = ind_green
        self.df_conn_red = df_conn_red
        self.df_conn_green = df_conn_green

    def connectivity_matrix(self):

        # Get centers of soma classes
        center_PC = np.median(np.where(self.df_conn.columns.get_level_values(0).values == 'PC'))/len(self.df_conn.columns)
        divide_PC_IN = (np.where(self.df_conn.columns.get_level_values(0).values == 'IN')[0][0]-0.5)/len(self.df_conn.columns)
        center_IN = np.median(np.where(self.df_conn.columns.get_level_values(0).values == 'IN'))/len(self.df_conn.columns)

        # Get colors
        df_conn_colors = []
        for i in range(len(self.df_conn)):
            if bool(self.ind_red.values[i]) is True:
                df_conn_colors.append((0.8, 0, 0, 1))
            else:
                df_conn_colors.append((0, 0.8, 0, 1))

        center_row = self.ind_red.idxmax()

        fig, ax = plt.subplots(figsize=(13, 6))

        im = ax.imshow(self.df_conn, cmap='Blues')
        ax.set_yticks(np.arange(len(self.df_conn)))
        ylabels = list(self.df_conn.index.values)
        for idx in range(len(ylabels)):
            if (idx + 1) % 2 == 0:
                ylabels[idx] = ylabels[idx] + '                '
        ax.set_yticklabels(ylabels)
        for idx, yticklabel in enumerate(ax.get_yticklabels()):
            yticklabel.set_color(df_conn_colors[idx])
        ax.tick_params(axis="y", labelsize=7)
        ax.set_xticks(np.arange(len(self.df_conn.columns)))
        xlabels = list(self.df_conn.columns.get_level_values(1).values)
        for idx in range(len(xlabels)):
            xlabels[idx] = '{:03.0f}'.format(xlabels[idx])
            if (idx + 1) % 2 == 0:
                xlabels[idx] = xlabels[idx] + '        '
        ax.set_xticklabels(xlabels, rotation=90)
        ax.tick_params(axis="x", labelsize=7)
        divider = make_axes_locatable(ax)
        cax_right = divider.append_axes("right", size="1%", pad=0.05)
        fig.colorbar(im, cax=cax_right, ticks=list(range(5)), values=list(range(5)))
        cax_bottom = divider.append_axes("bottom", size="1.5%", pad=0.65)
        cax_bottom.spines['top'].set_visible(False)
        cax_bottom.spines['right'].set_visible(False)
        cax_bottom.spines['bottom'].set_visible(False)
        cax_bottom.spines['left'].set_visible(False)
        cax_bottom.set_xticks([center_PC, center_IN])
        cax_bottom.set_xticks([0, divide_PC_IN, 1], minor=True)
        cax_bottom.set_xticklabels(['PC', 'IN'])
        cax_bottom.set_xlim(0,1)
        cax_bottom.tick_params(axis="x", labelsize=5, length=0)
        cax_bottom.set_yticklabels([])
        cax_bottom.tick_params(axis="y", labelsize=5, length=0)

        plt.savefig(self.output_path.joinpath('connectivity_matrix.pdf').as_posix())

    def connectivity_graph(self):

        graph = Graph(self.db)
        pos = graph.get_layout_tripartite()
        output_path = self.output_path.joinpath('connectivity_graph_tripartite.pdf').as_posix()
        graph.draw(figsize=(16,12),output_path=output_path, pos=pos)

    def cluster_specificity(self):

        community_ids = set(list(self.c.values()))

        df_cluster = pd.DataFrame(columns=['num_axons', 'num_targets', 'sum_green', 'sum_red', 'specificity_score',
                                           'sum_green_unique', 'sum_red_unique', 'specificity_score_unique'],
                                  index=community_ids)

        df_cluster.index.name = 'cluster id'

        for community_id in community_ids:
            col_inds = np.where(self.df_ep['community_id'] == community_id)[0]

            row_inds_green = \
                np.where((self.df_ax['community_id'] == community_id) & (self.df_ax['axon_label_color'] == 'green'))[0]
            row_inds_red = \
                np.where((self.df_ax['community_id'] == community_id) & (self.df_ax['axon_label_color'] == 'red'))[0]

            print('community id {}'.format(community_id))
            print(self.df_ax['axon_id'].values[row_inds_green])
            print(self.df_ax['axon_id'].values[row_inds_red])
            print(self.df_ep['endpoint_id'].values[col_inds])

            data_green = self.df_conn.values.copy()
            data_green = data_green[row_inds_green, :]
            data_green = data_green[:, col_inds]
            sum_green = int(np.sum(data_green))
            sum_green_unique = int(np.sum(data_green > 0))

            data_red = self.df_conn.values.copy()
            data_red = data_red[row_inds_red, :]
            data_red = data_red[:, col_inds]
            sum_red = int(np.sum(data_red))
            sum_red_unique = int(np.sum(data_red > 0))

            specificity_score = np.round(max([sum_green, sum_red]) / (sum_green + sum_red), 2)
            specificity_score_unique = np.round(max([sum_green_unique, sum_red_unique]) / (sum_green_unique + sum_red_unique), 2)

            df_cluster.loc[community_id, 'num_axons'] = len(row_inds_green) + len(row_inds_red)
            df_cluster.loc[community_id, 'num_targets'] = len(col_inds)
            df_cluster.loc[community_id, 'sum_green'] = sum_green
            df_cluster.loc[community_id, 'sum_red'] = sum_red
            df_cluster.loc[community_id, 'specificity_score'] = specificity_score
            df_cluster.loc[community_id, 'sum_green_unique'] = sum_green_unique
            df_cluster.loc[community_id, 'sum_red_unique'] = sum_red_unique
            df_cluster.loc[community_id, 'specificity_score_unique'] = specificity_score_unique

        df_cluster.to_csv(self.output_path.joinpath('cluster_specificity.csv').as_posix())
        df_cluster.to_latex(self.output_path.joinpath('cluster_specificity.tex').as_posix())

    def stats(self):
        n_synapses_green = np.sum(self.df_conn_green.values)
        n_synapses_green_PC = np.sum(self.df_conn_green['PC'].values)
        n_synapses_green_IN = np.sum(self.df_conn_green['IN'].values)
        n_connections_green = np.sum(self.df_conn_green.values > 0)

        n_synapses_red = np.sum(self.df_conn_red.values)
        n_synapses_red_PC = np.sum(self.df_conn_red['PC'].values)
        n_synapses_red_IN = np.sum(self.df_conn_red['IN'].values)
        n_connections_red = np.sum(self.df_conn_red.values > 0)


















