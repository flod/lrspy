import numpy as np
from numpy.core.multiarray import ndarray
from matplotlib import colors
from scipy.stats import binom


def generate_hue_matrix_rgb(r_max=10, g_max=10, r_ints=None, g_ints=None, color_modifier=None):
    """
    Generate a matrix composed of different mixtures of red and green.

    Args:
        r_max (int): number of red hues
        g_max (int): number of green hues
        r_ints (list of float): intervals of red hues
        g_ints (list of float): intervals of green hues
        color_modifier: function modifying the hues. default is a modified log function

    Returns:
        hm: Hue matrix
    """

    if color_modifier is None:
        def color_modifier(x):
            #c = x / max(x) # works for all ranges
            #c = (1 / (1 + np.exp(-x)) - 0.5) * 2 # works for [0,5]
            c = (1 / (1 + np.exp(-x / 2.5)) - 0.4) * 1.71 # works for [0,10]
            return c

    if r_ints is None:
        hm_rgb = np.zeros((r_max + 1, g_max + 1, 3))
    else:
        hm_rgb = np.zeros((len(r_ints), len(g_ints), 3))

    if r_ints is None:
        r_cols = color_modifier(np.arange(r_max + 1)).reshape(r_max + 1, -1)
        r_colm = np.tile(r_cols, (1, g_max + 1))
    else:
        r_cols = color_modifier(r_max * r_ints).reshape(len(r_ints), -1)
        r_colm = np.tile(r_cols, (1, len(g_ints)))

    if g_ints is None:
        g_cols = color_modifier(np.arange(g_max + 1)).reshape(-1, g_max + 1)
        g_colm = np.tile(g_cols, (r_max + 1, 1))
    else:
        g_cols = color_modifier(g_max * g_ints).reshape(-1, len(g_ints))
        g_colm = np.tile(g_cols, (len(r_ints), 1))

    if g_ints is None:
        b_colm = np.zeros((r_max + 1, g_max + 1))
    else:
        b_colm = np.zeros((len(r_ints), len(g_ints)))

    hm_rgb[:, :, 0] = r_colm
    hm_rgb[:, :, 1] = g_colm
    hm_rgb[:, :, 2] = b_colm

    return hm_rgb


def generate_hue_matrix_hex(r_max=10, g_max=10, r_ints=None, g_ints=None, color_modifier=None):
    """
    Generate a matrix composed of different mixtures of red and green.

    Args:
        r_max (int): number of red hues
        g_max (int): number of green hues
        r_ints (list of float): intervals of red hues
        g_ints (list of float): intervals of green hues
        color_modifier: function modifying the hues. default is a modified log function

    Returns:
        hm: Hue matrix
    """

    if color_modifier is None:
        def color_modifier(x):
            #c = x / max(x) # works for all ranges
            #c = (1 / (1 + np.exp(-x)) - 0.5) * 2 # works for [0,5]
            c = (1 / (1 + np.exp(-x / 2.5)) - 0.4) * 1.71 # works for [0,10]
            return c

    if r_ints is None:
        hm_hex = np.empty((r_max + 1, g_max + 1), dtype='|S24')
    else:
        hm_hex = np.empty((len(r_ints), len(g_ints)), dtype='|S24')

    if r_ints is None:
        r_cols = color_modifier(np.arange(r_max + 1)).reshape(r_max + 1, -1)
    else:
        r_cols = color_modifier(r_max * r_ints).reshape(len(r_ints), -1)

    if g_ints is None:
        g_cols = color_modifier(np.arange(g_max + 1)).reshape(-1, g_max + 1)
    else:
        g_cols = color_modifier(g_max * g_ints).reshape(-1, len(g_ints))

    for idx, _ in np.ndenumerate(hm_hex):
        hm_hex[idx[0],idx[1]] = colors.rgb2hex((r_cols[idx[0],0], g_cols[0,idx[1]], 0))

    return hm_hex


def generate_specifity_matrix(counts_green, counts_red, table_size: tuple=None, suppress_origin: bool=True):
    """
    Generate specificity histogram matrix

    Args:
        counts_green: Specifies number of red innervation counts
        counts_red: Specifies number of green innervation counts
        table_size: Determines upper limit of matrix dimensions
        suppress_origin: If true, sm[0,0] != 0

    Returns:
        sm: Specificity matrix
    """
    if table_size is None:
        sm: ndarray = np.zeros((int(max(counts_green)) + 1, int(max(counts_red)) + 1))
    else:
        sm: ndarray = np.zeros((table_size[0], table_size[1]))

    for countIdx, _ in enumerate(counts_green):
        hit_count_0 = int(counts_green[countIdx])
        hit_count_1 = int(counts_red[countIdx])
        sm[hit_count_0, hit_count_1] = sm[hit_count_0, hit_count_1] + 1

    # Set [0,0] := 0
    if suppress_origin:
        sm[0, 0] = 0

    return sm


def generate_binom_prob_matrix(x_max_green, x_max_red, theta):

    pm = np.zeros((x_max_red+1, x_max_green+1))

    for idx, _ in np.ndenumerate(pm):
        x = idx[0]
        n = idx[0] + idx[1]
        #p = 1 - binom.cdf(x - 1, n, theta)
        p = binom(n, theta).pmf(x)
        pm[idx] = p

    return pm

def generate_binom_freq_matrix(pm, sm):

    fm = np.zeros(sm.shape)
    for n in range(1, sm.shape[0]):
        n_count = 0
        for (i, j), z in np.ndenumerate(sm):
            if (i + j == n):
                n_count = n_count + z
        for (i, j), z in np.ndenumerate(pm):
            if (i + j == n):
                fm[i, j] = np.round(pm[i, j] * n_count).astype(int)

    return fm
