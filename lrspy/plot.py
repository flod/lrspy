import numpy as np
from matplotlib import pyplot as plt
from numpy.core.multiarray import ndarray
from scipy.stats import kde
from sklearn.neighbors import KernelDensity

def soma_specificity_scatter(ax, xy_data, xy_range, z_data, z_range, color_data, layer_data=None, class_data=None,
                             marker_size=2, linewidths=0.5, density=False, density_params=None, static_lims=False):

    if class_data is None:
        class_data = ['None' for i in range(len(xy_data))]

    edgecolors = []
    for idx, cl in enumerate(class_data):
        if cl == 'IN':
            edgecolors.append('#000000')
        else:
            edgecolors.append(color_data.values[idx])

    if density is False:
        ax.scatter(xy_data, z_data, c=color_data, s=marker_size, linewidths=linewidths, edgecolors=edgecolors)
    else:
        h = ax.hist2d(xy_data, z_data,
                      bins=[list(range(xy_range[0], xy_range[1]+1, 100)), list(range(z_range[0], z_range[1]+1, 100))],
                      cmap=density_params['cmap'], vmin=0, vmax=density_params['vmax'])
        plt.colorbar(h[3], ax=ax, ticks=list(range(0, int(density_params['vmax']+1), 5)), values=list(range(int(density_params['vmax']+1))))

    if layer_data is not None:
        if static_lims is False:
            xlim = ax.get_xlim()
        else:
            xlim = xy_range
        for _, row in layer_data.iterrows():
            z = row.z
            ax.plot([xlim[0], xlim[1]], [z, z], c=(0.5, 0.5, 0.5, 1), linewidth=0.5)

    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.set_xlim(xy_range[0], xy_range[1])
    ax.set_ylim(z_range[0], z_range[1])
    return ax

def soma_specificity_kde(ax, z_data, z_range, c_data, line_width = 0.5):
    kdes = []
    lns = []
    x_plot = np.arange(z_range[0], z_range[1]).reshape(-1, 1)
    c_data_unique = c_data.unique()
    for c_unique in c_data_unique:
        z_data_c = np.asarray(z_data[c_data == c_unique]).reshape(-1, 1)
        kde = KernelDensity(kernel='gaussian', bandwidth=20).fit(z_data_c)
        kde_log_norm = kde.score_samples(x_plot)
        kde = np.exp(kde_log_norm) * z_data_c.shape[0]
        kdes.append(kde)
        ln = ax.plot(kde, x_plot, color=c_unique, linewidth=line_width)
        lns.append(ln)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    ax.set_ylim(z_range[0], z_range[1])

    return ax, kdes

def soma_specificity_axes_grid(num_x, num_y, spacing_x=0.05, spacing_y=0.05, split_x=True, split_x_ratio=0.8):
    tmp = np.linspace(0 + spacing_y,1,num_y+1)
    pos_y_min = np.flip(tmp[:-1])
    pos_y_max = np.flip(tmp[1:] - spacing_y)

    tmp = np.linspace(0 + spacing_x,1,num_x+1)
    pos_x_min = tmp[:-1]
    pos_x_max = tmp[1:] - spacing_x

    if split_x == True:
        pos_x_min = np.repeat(pos_x_min, 2)
        pos_x_max = np.repeat(pos_x_max, 2)
        width_x = pos_x_max - pos_x_min
        width_x_split = split_x_ratio * width_x
        width_x_split[::2] = 0
        pos_x_min = pos_x_min + width_x_split
        width_x_split = (1-split_x_ratio) * width_x
        width_x_split[1::2] = 0
        pos_x_max = pos_x_max - width_x_split

    mesh_x_min, mesh_y_min = np.meshgrid(pos_x_min, pos_y_min)
    mesh_x_max, mesh_y_max = np.meshgrid(pos_x_max, pos_y_max)

    ax_grid: ndarray = np.empty((mesh_x_min.shape[0], mesh_x_min.shape[1]), dtype=object)
    for idx, _ in np.ndenumerate(mesh_x_min):
        left = mesh_x_min[idx[0],idx[1]]
        bottom = mesh_y_min[idx[0],idx[1]]
        width = mesh_x_max[idx[0],idx[1]] - mesh_x_min[idx[0],idx[1]]
        height = mesh_y_max[idx[0],idx[1]] - mesh_y_min[idx[0],idx[1]]
        ax = plt.axes([left, bottom, width, height])
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        ax_grid[idx[0],idx[1]] = ax

    return ax_grid
